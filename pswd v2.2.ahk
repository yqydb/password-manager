#Include Crypt.ahk
#Include CryptConst.ahk
#Include CryptFoos.ahk
#Persistent
#SingleInstance


/**
 * hrms shift clalim 
 * @author hul
 * @version 1.0
 * @change: init
 * @created 2014-8-23




 */


/**
 * password manager
 * @author hul
 * @version 2.1
 * @change: add ftp submit function. read the config from config file, and change password from mainframe
 * @created 2014-4-28
 */

/**
 * password manager
 * @author hul
 * @version 2.2
 * @change: remove dhcp switch function, add password history.
 * @created 2015-9-18
 */



;Crtl+Win+P is to invoke maint panel

;Read config.ini

;~ set the max number of password allow to use.
Global  g_Pswd_Filename,g_Function,g_Key,g_Description, g_Rule, g_Interval, g_Description, g_Userid, g_email, g_Masterpswd,g_update,g_update_Interval, g_update_Description,g_inter_choose,g_used_1,g_used_2,g_used_3,g_used_4,g_used_5,g_used_6,g_used_7,g_used_8



Global  MAX_PASSWORD,NOTIFY_DAYS,LAST_NOTIFY_DAYS , FUNTION_CREATE, FUNTION_REFRESH,FUNTION_VIEW,FUNTION_VIEWLAST,FUNTION_UPDATE, FUNTION_REMOVE,FUNTION_VIEWEMAIL,FUNTION_SETEMAIL,FUNTION_VIEWMASTER,FUNTION_SETMASTER

MAX_PASSWORD 			= 8
NOTIFY_DAYS  			= 5
LAST_NOTIFY_DAYS   		= 1


FUNTION_CREATE      	= 1
FUNTION_SET				= 2
FUNTION_REFRESH    		= 3
FUNTION_VIEW    		= 4
FUNTION_VIEWLAST		= 5
FUNTION_UPDATE  		= 6
FUNTION_REMOVE			= 7
FUNTION_VIEWEMAIL		= 8
FUNTION_SETEMAIL		= 9
FUNTION_SETMASTER		= 10
FUNTION_SENDEMAIL		= 11
FUNTION_DECRYPTFILE	= 12

PUBLIC                      := "PASSWORD MASTER"
SEND_FILE					:= "pswd.enc"yyNfxH3J

Global  MASTER_CONFIG_FILE,  PUBLIC, MASTER_CONFIG_FILE_CR,ENCRYPT_FILE, DNS 

;reset dns to dhcp
; remove dhcp_function 
;RunWait netsh interface ip set dns Wireless source=dhcp
;dns = dhcp


MASTER_CONFIG_FILE = %A_ScriptDir%\AHKpswd.cfg
ENCRYPT_FILE   =  %A_ScriptDir%\pswd.enc



f_Config_Read()
IniWrite, NO, %MASTER_CONFIG_FILE%, Password, Verify
f_Verfiy_Master()

;MsgBox Press hotkey Win+P to invoke main panel Win+Fn to output Password


menu, tray, NoStandard
menu, tray, add, MainPanel
menu, tray, add, Help
menu, tray, add, About
menu, tray, add, Exit 
menu, tray, Default, MainPanel

#p::

Begin:	
Gui, Destroy
While true {
	f_Config_Read()
	f_Check_expire()
	f_Create_MasterPanel()	
	return
}


#F1::
	g_key := 1

	f_Verify_verified( )
		
	if  f_Read_Password(pswd) = -1
		exit 
	f_Check_expire()
	SendRaw %pswd%	
	exit 

#F2::
	g_key := 2
	
	f_Verify_verified()
	
	if f_Read_Password(pswd) = -1
		exit	
	f_Check_expire()
	SendRaw %pswd%	
	exit 

#F3::
	g_key := 3
	
	f_Verify_verified()
	
	if f_Read_Password(pswd) = -1
		exit
	f_Check_expire()
	SendRaw %pswd%
	exit 
	
#F4::
	g_key := 4
	
	f_Verify_verified()
	
	if f_Read_Password(pswd) = -1
		exit
	f_Check_expire()
	SendRaw %pswd%
	exit 
	
#F5::
	g_key := 5
	f_Verify_verified()
	if f_Read_Password(pswd) = -1
		exit
	f_Check_expire()
	SendRaw %pswd%
	exit 
	
#F6::
	g_key := 6
	f_Verify_verified()
	if f_Read_Password(pswd) = -1
		exit
	f_Check_expire()
	SendRaw %pswd%
	exit 

#F7::
	g_key := 7
	f_Verify_verified()
	if f_Read_Password(pswd) = -1
		exit
	f_Check_expire()
	SendRaw %pswd%
	exit 
	
#F8::
	g_key := 8
	f_Verify_verified()
	if f_Read_Password(pswd) = -1
		exit
	f_Check_expire()
	SendRaw %pswd%
	exit 
	
#j::	
	run "%A_ScriptDir%\Jdate.exe"
	Exit	

#h::	
	f_display_help()
	Exit

#q::
send 2.00
sleep 100
send {tab}
sleep 100
send 21:00
send {tab}
sleep 100
send 23:00

return

#e::
send 8.00
sleep 100
send {tab}  
sleep 100
send 14:00
send {tab}
sleep 100
send 22:00

return


#w::
send 8.00
sleep 100
send {tab}
sleep 100
send 05:00
send {tab}
sleep 100
send 14:00

return

f_Config_Read()  
{
	IfNotExist  %MASTER_CONFIG_FILE% 
	{
	;~  First specify password filename, then create AhKpswd.cfg
		MsgBox, 4,, this is your first time to use Password Manager, please read readme carefully
		IfMsgBox, Yes
			f_display_help()	
			
		
		MsgBox, 4, , Specify your Password file to continue. It can be non-existing.
		IfMsgBox, No   			; no password file, exit 
			ExitApp
			;~ specify the password fie
		
		
		FileSelectFile, g_Pswd_Filename, S8	,%A_ScriptDir%	
		if  g_Pswd_Filename=		
		{	
			MsgBox, 4,, password filename not specified, try it now?	
			IfMsgBox, No   
				ExitApp
			FileSelectFile, g_Pswd_Filename, S8,%A_ScriptDir%
			if  g_Pswd_Filename = 
				ExitApp
		}
		
		f_Verfiy_Master()
		
		IniWrite, %g_pswd_filename%, %MASTER_CONFIG_FILE%, Password, Directroy
		IniWrite, %A_Now%, %g_Pswd_Filename%,Init, Time	
		IniWrite, %g_pswd_filename%, %g_Pswd_Filename%,Init, Directory	
	return
		
					
	}
	
	IniRead, g_Pswd_Filename, %MASTER_CONFIG_FILE%, Password, Directroy
	
	if  g_Pswd_Filename=		
	{	
		MsgBox, 4,, password filename not specified, try it now?		; one more chanace to say YES
		IfMsgBox, No   
			Exit

		FileSelectFile, g_Pswd_Filename, S8
		if  g_Pswd_Filename = 
			Exit
	
		;~ write password file name to config file	
		IniWrite, %g_Pswd_Filename%, %MASTER_CONFIG_FILE%, Password, Directroy
		return
	}

	;~ IniRead,Master_Pswd,%MASTER_CONFIG_FILE%,Master,Security,Master
	;~ if  Master_Pswd =""{			
		;~ MsgBox,4,, You haven't 
	
	;~ Read password file name from config file	
	
	return  
}


f_display_help(){
x1 = 
(
First Run Password Manager instruction
    
* Specify a password file directory for the first time 
	Password file can be non-existing
	or an existing file if you already have
   
* Specify a master password.
	Make sure your password file is safe and can be backed up easily. 
	Enter the same master password if you have already set
	Master password can be any character, any length, and no expiration days.  
	For safety reason, it should be complicate enough and change it regularly.     
	
* HotKey driven
	Win+P to invoke main panel
	Win+Fn to output corresponding password
	Win+H to invoke help panel
 )

x2  = 
(
 I. Create  password 
8 password slots are provided, with correspond with Win+ F1,F2..F8 to output is provided. 

* Fill the Description, USERID  
	Both are optional.  
	It will be very useful if you enter meaningful some words in it when     you have to change the expiring password!  
	Description can be account, website or application name.  

* Select a rule best match for your password.  
	The available rules are:
	6 char  Aa1***		Good for a 6 character password; 
	8 char  Aa1*****		Most common password rule, can satisfy majority requirement. 
	12 char Aa1*********  	Stronger in length, use this rule if a 12 length password is allowed. Usually used for website password! 
	12 char Aa1@********  	strongest password rule by using any special character: !,@,#,$,&,*,(,)	 	

* Select an interval best match for your password.  
	Available interval can be 1 month, 2 months, 3 months, 1 year, never; 
	Using never if password is expected as never expire.  
	This only effect the reminder function, you still can refresh password even your interval is set to never. 

* Red char M S E indicates 
	M	-   Multiple uses. It has been changed at least once.  
	S	-   Single use. Password has been created and never changed before 
	E	-   Password is going to Expire     
	X	-   Password is eXpired.
)

x3  =      
(
II. Set Password  
	Manually set your password with no rule restriction. (Not recommended)
	you can't refresh a manually set password s

III. Refresh Password 
	Refresh your password if it's about to expire

IV. View Password 
	View all your password information,  
	including when password has been created, the rule, interval, description, userid. 
	
V. View Last Password 
	View last password, and directly copy to your clipboard. 

VI. Update Password 
	Update your password information.  
	You can update Description, Userid.  
	You can't update rule, interval after the password has been created. Remove and re-create it. 

VII. Remove Password 
	Remove all the specific password information, and make it available for reuse. 
)

X4 = 
(
		MISC functions
VIII. View email 
	View your email address previous set. 

IX. Set email 
	Set a new email address.  
	Before you can use mail backup, a Microsoft office outlook client as well as a well-config mail account are required  
	Config a GMAIL account, click here. Config a QQ mail account, click here.  

X. Set master 
	Set or overwrite master password 

XI. Send password file 
 
	Send password file to your destination mailbox.  
	The file has been encrypted before attached to mail 

XII. Decrypt file 
	After you download your password file from email, you need decrypt it first before you can use. 
	Decrypt password is your master password 
	
XIII. swtich DNS.
    press dhcp/att button to switch the DNS enviorment. default is dhcp, if u're using att, and want to add dns 9.0.148.50, press dhcp.
	
XIV. claim shift
    win+q claim 16:00~00:00, win+w claim 22:00~6:00
	
)

MsgBox  ,1,,%x1%,300

IfMsgBox OK
	MsgBox  ,1,,%x2%,300

IfMsgBox OK
	MsgBox  ,1,,%x3%,300

IfMsgBox OK
	MsgBox  ,,,%x4%,300


return
}

f_display_readme(){
 
	run "%A_ScriptDir%\readme.doc"
	sleep  3000
   
}

f_Verfiy_Master(){
	IniRead, l_Read_Master, %MASTER_CONFIG_FILE%, Password, Master
	/*
	if ((l_Read_Master="")or (l_Read_Master = "ERROR")or(l_Read_Master = 0)) 
		InputBox, l_Userid, Create User ID,  _=========== Create User ID =========_`n `nNOTE: `n Remember your ID, it's required while logon   , , 400, 300
	*/
	IniRead, l_Read_Master, %MASTER_CONFIG_FILE%, Password, Master
	if ((l_Read_Master="")or (l_Read_Master = "ERROR")or(l_Read_Master = 0)) {
		InputBox, l_Master, Enter User ID & Master Password,  _=========== Enter  master Password =========_`n `nNOTE: `n 1. Enter your New Master password.`n 2. Don't use arbitrary one`, it's the key to access all your Password`n 3. If you have set master Password before`, enter the same one `n 4.Your config file can be read and used on any PC with your correct Master Password.   ,HIDE , 400, 300
	
		if (l_Master="") {
			MsgBox You have to set master password to continue, exit now...
			ExitApp
		}
		
		InputBox, l_Master_Sec, Enter Master Password, Confirm your Master Password ,HIDE , 200, 150
		if (l_Master_Sec<>l_Master){
			MsgBox Master password misatch, exit now...
			ExitApp				
		}
		
		
		l_Masterpswd_enc:= Crypt.Encrypt.StrEncrypt(l_Master,PUBLIC,5,1) 	
		if  ((l_Masterpswd_enc ="")or (l_Masterpswd_enc= "ERROR")or(l_Masterpswd_enc = 0)) {
			MsgBox Encode Master password failed. %l_Masterpswd_enc%
			ExitApp
		}
		IniWrite, %l_Masterpswd_enc%, %MASTER_CONFIG_FILE%, Password, Master
		MsgBox Master Password  created, You need it to do everything.
		
	}
	else {
		InputBox, l_Verify_Master, Enter Master Password, Verify Master Password please enter to continue.,HIDE , 200, 150
		l_Masterpswd_enc:= Crypt.Encrypt.StrEncrypt(l_Verify_Master,PUBLIC,5,1) 	
		if (l_Masterpswd_enc<>l_Read_Master){
			MsgBox Master Password mismatch.
			if (g_Function ="")  ;First time to verify, exit directly
				ExitApp
			else 				;invoke primary panel again.
				return -1
		}		
		IniWrite, YES, %MASTER_CONFIG_FILE%, Password, Verify
	}	
}

f_Verify_verified()
{
	IniRead, verified, %MASTER_CONFIG_FILE%, Password, Verify
	if verified <> YES 
	{
		MsgBox You have to verify your master first!
		f_Verfiy_Master()	
	}
}

f_check_decrypt(str,caller)
{
	if (str="") {
		MsgBox Decrypt failed during %caller%
		exit 	
	}
		
}

f_Check_expire()
{
		i = 0
	Loop , %MAX_PASSWORD%{
		i++
		l_current_stamp := A_Now
		IniRead, l_Stamp,%g_Pswd_Filename%,%i%,Stamp
		IniRead, l_interval,%g_Pswd_Filename%,%i%,Interval
		
		if ((l_interval = "") or(l_interval =0) or (l_Interval = "ERROR"))
			continue 
		
		if  (l_Interval = 4)   		;  selection is 1 year
			l_Interval = 12
	
		if (l_Interval = 5) 		; never expire	
			l_Interval = 9999	
	
		
		if ((l_Stamp <>"") and (l_Stamp <>"ERROR")){
			EnvSub, l_current_stamp, %l_Stamp%, days
			
			l_interval *= 30 		
						
			if 	(l_interval - l_current_stamp<= NOTIFY_DAYS and l_interval - l_current_stamp>LAST_NOTIFY_DAYS)  			;~ Give notify 5 days before expire
				MsgBox Password %i% is going to expired in less than 5 days, Refresh it now!			
			if  (l_interval - l_current_stamp < LAST_NOTIFY_DAYS ) 
				MsgBox password%i% is already expired, refresh the Password now.  
					;~ f_Send_mail()
			
		}				
	}
}
	

f_Check_Used(  )
{
	i = 0
	Loop , %MAX_PASSWORD%{
		i++		
		IniRead, l_pswd,%g_Pswd_Filename%,%i%,Password
		IniRead, l_prev_pswd,%g_Pswd_Filename%,%i%,Prev_password
		
				l_current_stamp := A_Now
		IniRead, l_Stamp,%g_Pswd_Filename%,%i%,Stamp
		IniRead, l_interval,%g_Pswd_Filename%,%i%,Interval
		
		if ((l_interval = "") or(l_interval =0) or (l_Interval = "ERROR")){
			g_used_%i% = na
			continue 
		}	
			
		
		if  (l_Interval = 4)   		;  selection is 1 year
			l_Interval = 12
	
		if (l_Interval = 5) 		; never expire	
			l_Interval = 9999	
	
		
		if ((l_Stamp <>"") and (l_Stamp <>"ERROR")){
			EnvSub, l_current_stamp, %l_Stamp%, days
			
			
			l_interval *= 30 	
			if ((l_pswd = "") or (l_pswd = "ERROR"))
				g_used_%i% = ""
			if ((l_prev_pswd = "") or (l_prev_pswd = "ERROR"))	
				g_used_%i% = S
			if  ((l_prev_pswd != "") and (l_prev_pswd != "ERROR") )
				g_used_%i% = M	
						
			
			if 	(l_interval - l_current_stamp<= NOTIFY_DAYS and l_interval - l_current_stamp>LAST_NOTIFY_DAYS)  
				g_used_%i% = E
			if (l_interval - l_current_stamp < LAST_NOTIFY_DAYS ) 
				g_used_%i% = X
		}		

	}
}

f_Create_MasterPanel( )
{	
	f_Check_Used( )
	Gui,1:Add, GroupBox,center w280 h245 , Password Master
	Gui,1: +MinimizeBox 
	Gui,1:Add, Text,x20 y25 cGray,&Function
	Gui,1:Font, w700 q5, Tahoma
	Gui,1:Add, DDL, AltSubmit x80 y25 w130 vg_function,  Create|Set|Refresh|View|ViewLast|Update|Remove|View Email|Set Email|Set Master|Send password file|Decrypt File 
	Gui,1:Font,,		
		
	Gui,1:Font,  q5  w550 s7	
 	Gui,1:Add, Text,x30 y50 cRed  ,%g_used_1%
 	Gui,1:Add, Text,x30 y70 cRed ,%g_used_2%
  	Gui,1:Add, Text,x30 y90 cRed ,%g_used_3%
  	Gui,1:Add, Text,x30 y110 cRed,%g_used_4%
  	
  	Gui,1:Add, Text,x150 y50 cRed,%g_used_5%
  	Gui,1:Add, Text,x150 y70 cRed,%g_used_6%
  	Gui,1:Add, Text,x150 y90 cRed,%g_used_7%
  	Gui,1:Add, Text,x150 y110 cRed ,%g_used_8%
	Gui,1:Font,,		
	
		
	Gui,1:Add, Radio, x45 y50 vg_Key, Password &1
	Gui,1:Add, Radio, x45 y70, Password &2
	Gui,1:Add, Radio, x45 y90, Password &3
	Gui,1:Add, Radio, x45 y110, Password &4
	Gui,1:Add, Radio, x165 y50 , Password &5
	Gui,1:Add, Radio, x165 y70, Password &6
	Gui,1:Add, Radio, x165 y90, Password &7
	Gui,1:Add, Radio, x165 y110, Password &8
		

	Gui,1:Add, GroupBox, x15 y130 w270 h115, 
	Gui,1:Add, Text,x20 y145 cGray,&Description
	Gui,1:Add, Edit,x80  y145 r1 h15 w200  vg_Description,              
	Gui,1:Add, Text,x20 y170 cGray,&Userid
	Gui,1:Add, Edit,x80  y170 r1 h15 w200  vg_Userid,              
	Gui,1:Add, Text,x20 y195 cGray,&Rule
	Gui,1:Add, DDL, AltSubmit x80 y195 w200 vg_Rule,  6 char  Aa1*** |8 char  Aa1***** |12 char  Aa1********* |12 char  Aa1@********
	Gui,1:Add, Text,x20 y220 cGray,&Interval
	Gui,1:Add, DDL, x80 y220 AltSubmit w200 vg_Interval %g_inter_choose%,  1 month |2 months |3 months |1 year |Never	
/* 		
 * 	Gui,1:Add, Text,x20 y245 cGray,&Email
 * 	Gui,1:Add, Edit,x80  y245 r1 h15 w200  vg_Email,              
 * 		
 * 	Gui,1:Add, Text,x20 y270 cGray,&Master PSW
 * 	Gui,1:Add, Edit,x80  y270 r1 h15 w200 Password vg_Masterpswd, 
 */

		;~ Gui, Font,w700 , Arial
	Gui, Font, s8
	Gui,1:Add, Button,x10 y260,OK	
	Gui,1:Add, Button,x40 y260 ,View
	;Gui,1:Add, Button,x71 y260 ,Min
	;Gui,1:Add, Button,x107 y260 ,Help
	;Gui,1:Add, Button,x80 y260 ,%dns%
	Gui,1:Add, Button,x120 y260 ,Exit
	Gui,1:Add, Button,x80 y260 ,Set	
	
	
	Gui,1:Font,  q5  w550	
	Gui,1:Add, Text,x230 y255 ,Clark Hu
	Gui,1:Add, Text,x200 y270 ,23465446@qq.com
	Gui,1:Font,,		
	
	Gui 1:-Border
	Gui,1:Margin, 5, 10
	Gui,1:Show,AutoSize Center,Password master
	return	
}

f_create_UpdatePanel()
{	
	
	IniRead, l_decp,%g_Pswd_Filename%,%g_Key%,Description	
	IniRead, l_interval,%g_Pswd_Filename%,%g_Key%,Interval


	Gui,2:Add, GroupBox,center w220 h140 , Update Password %g_Key%						
	Gui,2:Add, Radio, x20 y25 vg_update, Description 
	Gui,2:Add, Radio, x20 y40, Interval
	Gui,2:Add, Text,x20 y70 cGray,&Description
	Gui,2:Add, Edit,x80  y70 r1 h15 w200  vg_update_Description,%l_decp%            				
	Gui,2:Add, Text,x20 y95 cGray,&Interval	
	
	Gui,2:Add, DDL, x80 y95 AltSubmit w200 vg_update_Interval  Choose%l_interval%,  1 month |2 months |3 months |1 year |Never			


	;~ Gui, Font,w700 , Arial
	Gui,2:Add, Button,x20 y110 , Update
	Gui,2:Add, Button,x80 y110 , Cancel
	
	Gui 2:-Border
	;Gui,2:Margin, 5, 10
	Gui,2:Show,AutoSize Hide


	
	return	
}

f_set_password()
{
	
	IniRead, l_pswd,%g_Pswd_Filename%,%g_Key%,Password
	IniRead, l_decp,%g_Pswd_Filename%,%g_Key%,Description


	if  (((l_pswd = "ERROR") or (l_pswd = "" )) and ((l_decp= "ERROR") or (l_decp="" )))  {     	; password haven't create before
		
		
		
		
		
		InputBox, l_New_pswd, Enter New Password, Enter New  password.,Hide , 150, 150
		if ( l_new_pswd = ""){
			MsgBox New password can't be empty!
			return
		}
		
		InputBox, l_New_pswd_again, Verfiy new password, Enter new password again., Hide, 150, 150
			;~ verfiy password
			if (l_New_pswd_again <>l_New_pswd){
				MsgBox Password mismatch, try again						
				return
			}
		
		IniRead, Mpswd, %MASTER_CONFIG_FILE%, Password, Master		
		
		;~ msgbox  "master password"  %Mpswd%
		Mpswd_de := Crypt.Encrypt.StrDecrypt(Mpswd,PUBLIC,5,1)		
		
		l_new_pswd:= Crypt.Encrypt.StrEncrypt(l_New_pswd,Mpswd_de,5,1) ; encrypts string using AES_128 encryption and MD5 hash
		
		MsgBox Password has been saved. use Win+F%g_key% to auto type.
		IniWrite, %l_new_pswd%, %g_Pswd_Filename%,%g_Key%, Password	
		IniWrite, %A_Now%, %g_Pswd_Filename%,%g_Key%, Stamp	
		
		; rule = M means manual set password!
		g_Rule = M
		
		f_Write_Description()  		
		f_Write_Userid()  		
		f_Write_Interval()
		f_Write_Rule()
	}
	else 
		MsgBox   use REFRESH to overwrite the existing password! Or `n       use REMOVE the existing password and reCREATE
	return
}

f_create_password()
{
	
	IniRead, l_pswd,%g_Pswd_Filename%,%g_Key%,Password
	IniRead, l_decp,%g_Pswd_Filename%,%g_Key%,Description


	if  (((l_pswd = "ERROR") or (l_pswd = "" )) and ((l_decp= "ERROR") or (l_decp="" )))  {     	; password haven't create before
		f_Write_Password()	
		f_Write_Description()  		
		f_Write_Userid()  
		f_Write_Interval()
		f_Write_Rule()
		f_Read_Password(l_new_pswd)
		MsgBox   Password %g_Key% is %l_new_pswd%, interval is %g_Interval%
	}
	else 
		MsgBox   use REFRESH to overwrite the existing password! Or `n       use REMOVE the existing password and reCREATE
	return
}

f_Write_Password(){
	
	f_Generate_Password(pswd)
	
	
	If  not FileExist(g_pswd_filename) {
		MsgBox  write  doesn't exist,  Specify File Name Now
		FileSelectFile, g_pswd_filename, S8
		
		if  (g_pswd_filename = "" ) {
			MsgBox, You didn't select a folder.
			Exit
		}
		;~ save password file name to config file
		else	IniWrite, %g_pswd_filename%, %MASTER_CONFIG_FILE%, Password, Directroy
	}
	
	IniWrite, %pswd%, %g_Pswd_Filename%,%g_Key%, Password	
	IniWrite, %A_Now%, %g_Pswd_Filename%,%g_Key%, Stamp		
	;~ f_Send_mail( ) 	
 return  
}


f_Write_Description(){		
		
	if  ((g_Description != "ERROR") and (g_Description !=""))  {
		IniWrite, %g_Description%, %g_Pswd_Filename%, %g_Key%,  Description		
		;MsgBox Description for  password%g_Key%,Updated.	
	}	
	else  {
		MsgBox Description can't updated.  %g_Description%
		return -1
	}
		
	return
}

f_Write_Userid(){		
		
	if  ((g_Userid != "ERROR") and (g_Userid !=""))  {
		IniWrite, %g_Userid%, %g_Pswd_Filename%, %g_Key%,  Userid		
		;MsgBox Userid for  password%g_Key%,Updated.	
	}	
/* 	else  {
 * 		MsgBox USERID haven't been updated.  %g_Userid%
 * 		return -1
 * 	}
 */
		
	return
}

f_Write_Rule(){
	
	if ((g_Rule = "") or(g_Rule = "ERROR")){
		MsgBox Password Rule can't updated.  %g_Rule%
		Return -1
	}
	IniWrite, %g_Rule%, %g_Pswd_Filename%,%g_Key%, Rule
}

f_Write_Interval(){
	
	
	l_Interval := g_Interval	
	
	if  (l_Interval not in 1,2,3,4,5)
	{
		MsgBox Interval can't updated.  Its value is %l_interval%
		return
	}
	
	
	
	IniWrite, %l_Interval%, %g_Pswd_Filename%, %g_Key%,  Interval
		
	if  ((l_Interval = "ERROR") or (l_Interval ="")) 		
		MsgBox Interval can't updated.  %l_Interval%
	
	return
}

f_Read_Password(ByRef pswd){
	
	IniRead, encpswd,%g_Pswd_Filename%,%g_Key%,Password
	
	if ((encpswd = "")or(encpswd = "ERROR"))  {
		MsgBox Password has not been used yet
		return -1
	}
	
	IniRead, Mpswd, %MASTER_CONFIG_FILE%, Password, Master
	; decrypts the string using previously generated hash,AES_128 and MD5
	Mpswd := Crypt.Encrypt.StrDecrypt(Mpswd,PUBLIC,5,1) 					
	pswd := Crypt.Encrypt.StrDecrypt(encpswd,Mpswd,5,1)				  		
	return
}

f_View_Password(){
	
	IniRead, l_pswd,  %g_Pswd_Filename%, %g_Key%
	;~ Gui,Font, w1000 s18 q1, Tahoma
	if (l_pswd ="" ) 
		MsgBox Password %g_key% has not used.
	else 
		MsgBox  %l_pswd%
}				

;~ Read previous password
f_Read_PrePassword(){
	
	IniRead, encpswd,  %g_Pswd_Filename%, %g_Key%,Prev_password
	
	
	if ((encpswd = "")or(encpswd = "ERROR"))  {
		MsgBox Password do  not have previous record
		return -1
	}
	
	
	IniRead, Mpswd, %MASTER_CONFIG_FILE%, Password, Master
	
	; decrypts the string using previously generated hash,AES_128 and MD5
	Mpswd := Crypt.Encrypt.StrDecrypt(Mpswd,PUBLIC,5,1) 						
	f_check_decrypt(Mpswd,"read pre password decrypt Master password ")	
	
	pswd := Crypt.Encrypt.StrDecrypt(encpswd,Mpswd,5,1)				  
	f_check_decrypt(pswd,"decrypt password" %g_key%)	
	
			
	MsgBox old password has been copied into clipboard
	Clipboard  := pswd
	return
}

f_Remove_Password(){
	
	MsgBox, 4,,Sure you want to remove password? All the relate information will be removed.
	IfMsgBox,  No 	
	return
	
	IniRead, pswd,  %g_Pswd_Filename%, %g_Key%
	if (pswd ="") {
		MsgBox Password %g_key% hasn't been created before.
		return
	}
			
	IniDelete, %g_Pswd_Filename%, %g_Key%
	
	MsgBox  Password %g_Key% removed!
	return
}	

f_Generate_Password(ByRef Pswd){
	;~ Define all the character
	Char_1:="A"
	Char_2:="B"
	Char_3:="C"
	Char_4:="D"
	Char_5:="E"
	Char_6:="F"
	Char_7:="G"
	Char_8:="H"
	Char_9:="I"
	Char_10:="J"
	Char_11:="K"
	Char_12:="L"
	Char_13:="M"
	Char_14:="N"
	Char_15:="O"
	Char_16:="P"
	Char_17:="Q"
	Char_18:="R"
	Char_19:="S"
	Char_20:="T"
	Char_21:="U"
	Char_22:="V"
	Char_23:="W"
	Char_24:="X"
	Char_25:="Y"
	Char_26:="Z"
	Char_27:="a"
	Char_28:="b"
	Char_29:="c"
	Char_30:="d"
	Char_31:="e"
	Char_32:="f"
	Char_33:="g"
	Char_34:="h"
	Char_35:="i"
	Char_36:="j"
	Char_37:="k"
	Char_38:="l"
	Char_39:="m"
	Char_40:="n"
	Char_41:="o"
	Char_42:="p"
	Char_43:="q"
	Char_44:="r"
	Char_45:="s"
	Char_46:="t"
	Char_47:="u"
	Char_48:="v"
	Char_49:="w"
	Char_50:="x"
	Char_51:="y"
	Char_52:="z"
	Char_53:="0"
	Char_54:="1"
	Char_55:="2"
	Char_56:="3"
	Char_57:="4"
	Char_58:="5"
	Char_59:="6"
	Char_60:="7"
	Char_61:="8"
	Char_62:="9"
	Char_63:="!"
	Char_64:="@"
	Char_65:="#"
	Char_66:="$"
	Char_67:="%"
	Char_68:="&"
	Char_69:="*"
	Char_70:="("
	Char_71:=")"
	

	;~ Read config to check if rule already exist 
	IniRead, l_Rule,  %g_Pswd_Filename%, %g_Key%,Rule
	if ((l_Rule ="") or (l_Rule ="ERROR")) 
		l_Rule = %g_Rule%

	/*
	**************************************************************************************************************************************	
		Password rule:
		   1. Any password created begin with Upper Case, Lower Case, Numeric for the first 3 characters.
		   2. If password length is 6 or 8, then only contain alphabet and numeric
		   3. If password length is 12, then contain alphabet, numeric 
		   4. If password length is 12, then contain alphabet, numeric as well as special characters, like !,@,#,$,%,&,*,(,)
	**************************************************************************************************************************************			
	
	*/ 	
	if (l_Rule =1) 
		length = 6 
	if (l_Rule =2) 
		length = 8	
	if ((l_Rule =3)  or (l_rule=4))
		length = 12  
			
	Random,R1,1,26
	Random,R2,27,52
	Random,R3,53,62	
	C1:=Char_%R1%
	C2:=Char_%R2%
	C3:=Char_%R3%
	pswd =%C1%%C2%%C3%
			
	;~ Leave last char as capital 
	 length -=  4	 	

	
	Loop , %length% {
		if  (l_rule = 4)   	; 12 -3 		
			Random,x,1,71
		else          		   
			Random,x,1,62
		 
		c := char_%x%
				
		StringCaseSense , On
		pswd = %pswd%%c%		
	}
		
		Random,Rx,1,26
		c := char_%Rx%
				
		StringCaseSense , On
		pswd = %pswd%%c%		
		
		IniRead, Mpswd, %MASTER_CONFIG_FILE%, Password, Master		
		
		;~ msgbox  "master password"  %Mpswd%
		Mpswd_de := Crypt.Encrypt.StrDecrypt(Mpswd,PUBLIC,5,1)		
		
		pswd := Crypt.Encrypt.StrEncrypt(pswd,Mpswd_de,5,1) ; encrypts string using AES_128 encryption and MD5 hash
		;msgbox  "New password has been encrypets " %pswd%		
	
}


f_Check_Selection(){
	if  (g_Key < 1) {				; button not pressed correctly,
		MsgBox  PLEASE select one password to continue !!!	
		Gui, Destroy
		return -1
	}
}


f_Send_mail(Subject, Body){		
	IniRead, Toaddress,%MASTER_CONFIG_FILE%, Password, Email
	;~ IniRead, %%,%g_Pswd_Filename%,%g_Key%,			
	
	if ((Toaddress ="") or (Toaddress = "ERROR")) {
		MsgBox mail Adress fetch failed. Set correct Email address first.
		Gui, Destroy
		Exit
	}				


/* 	Subject = "Confidential Pin Code"
 * 	IniRead, Body,%g_Pswd_Filename%,%g_Key%				
 * 	AttachPath :=g_Pswd_Filename
 */

	/*
	object.FromAddress := "PasswordMaster@qq.com"
	ToAddress := "szhulei@cn.ibm.com"
	Subject := "MAPI TEST"
	Body := "This is a test of the MAPI hooked batch sender"
	AttachPath := "c:\hul\pswd.ini" ; can add multiple attachments, the delimiter is |
	*/	
	AttachPath = %ENCRYPT_FILE%
	
	ol := ComObjCreate("Outlook.Application")
	ns := ol.getNamespace("MAPI")
	ns.logon("","",true,false) ;get default MAPI profile
	newMail := ol.CreateItem(0) ;0 is Outlook constant 'olMailItem' (thanks Sinkfaze)
	newMail.Subject := Subject
	newMail.Body := Body
	Attach := newMail.Attachments
	Loop, Parse,AttachPath, |, %A_Space%%A_Tab%
	Attach.Add(A_LoopField)

	; validate the recipient, just in case...
	myRecipient := ns.CreateRecipient(ToAddress)
	myRecipient.Resolve
	
	If Not myRecipient.Resolved
		MsgBox "unknown recipient"
	Else {
		newMail.Recipients.Add(myRecipient)
		;newMail.Sender := FromAddress ;this would be the from field but does not work yet
		newMail.Send
		MsgBox mail sent!
	}	
	
	FileDelete Attachpath
	Return
}


	
ButtonOK:
	Gui, Submit  
	if  (g_Function = "") {				; not choose any funtion to perform
		MsgBox Select one function
		goto begin		
	}
		
	;~  g_Function  = 	1	Create
	;~ 					2	Refresh
	;~ 					3	View
	;~ 					4	ViewLast
	;~ 					5 	Update
	;~ 					6 	Remove		
	if  ((g_Function <= 6) ) {
		if  (f_Check_Selection( ) = -1) 			; check opinion
			goto begin
	}
	
	
	;~ Password maintain
		
	/* 	
		===============================================================
					Create  Password
		===============================================================
	*/
	if  (g_Function = FUNTION_CREATE) {
		
		
		if 	((g_Rule = "" ) or (g_Interval = ""))		{
			MsgBox Select the rule and interval before create password! 
			Gui, Destroy
			goto begin
		}		
		f_create_password() 			; create password
		Gui, Destroy
		Exit
	}		
		
	
	/* 	
		===============================================================
					Refresh  Password
		===============================================================
	*/

	if  (g_Function = FUNTION_REFRESH) {
	
		if (f_Verfiy_Master() = -1)
		goto begin
		
		IniRead, l_Rule,  %g_Pswd_Filename%, %g_Key%,Rule
		if  (l_Rule = "M" ){
			MsgBox You can't refresh a manual set password. Remove and  recreate it if you want to change.
			goto begin
		}
		
		IniRead, l_interval,  %g_Pswd_Filename%, %g_Key%,Interval
		if  (l_interval = "5" ){
			MsgBox, 4,, You set this password as never expire. `n     Are You sure you want to Refresh for  password %g_Key% even if  you 
			IfMsgBox  No
				goto begin
		}
			
			
		
		IniRead, l_stamp,%g_Pswd_Filename%,%g_Key%,Stamp				
		if ((l_stamp = "") or (l_stamp = "ERROR")) {
			MsgBox Password %g_key% not created before
			goto begin
		}		
			
		l_current = %A_Now%
							
			;~ Check if refresh password on the same day which is not allowed.
		EnvSub, l_current, %l_stamp%, days
											
		if  l_current <=5
			MsgBox , Careful! You  are going to refresh your Password in less than 5 days.
		
		MsgBox, 4,, Are You sure you want to Refresh for  password %g_Key%
		IfMsgBox  No
			goto begin
				
		IniRead, pswd_prev, %g_Pswd_Filename%,%g_Key%, Password						
		IniRead, stamp_prev, %g_Pswd_Filename%,%g_Key%, Stamp											
			
		if ((pswd_prev != "ERROR") and (pswd_prev !="")){									
			pswdLength := StrLen(pswd_prev)
			IniWrite, %pswd_prev%, %g_Pswd_Filename%,%g_Key%, Prev_password	; save the previous password and get password length
			IniWrite, %stamp_prev%, %g_Pswd_Filename%,%g_Key%, Prev_stamp
		}	
				
		f_Write_Password()
		
		MsgBox  Password %g_Key% refreshed!	
		
		Gui, Destroy
		Exit
	}


	/* 	
		===============================================================
					View  Password
		===============================================================
	*/

	if  (g_Function = FUNTION_VIEW) {
		
/* 		if (f_Verfiy_Master() = -1)
 * 			goto begin
 */
		
		;~ Description, updated date & time, Previous (interval, ID)
		f_View_Password()				
				
		Gui, Destroy
		Exit
	}


	/* 	
		===============================================================
					View Last Password
		===============================================================
	*/

	if  (g_Function = FUNTION_VIEWLAST) {
	;	
	;	if (f_Verfiy_Master() = -1)
	;		goto begin
		;~ Description, updated date & time, Previous (interval, ID)
		f_Read_PrePassword()				
		Gui, Destroy
		Exit
	}

	/* 	
		===============================================================
					Update Password
		===============================================================
	*/

	if  (g_Function = FUNTION_UPDATE) {
		
		if (f_Verfiy_Master() = -1)
			goto begin
		
		IniRead, l_pswd,%g_Pswd_Filename%,%g_key%,Password
		IniRead, l_decp,%g_Pswd_Filename%,%g_key%,Description
	 	IniRead, l_userid,%g_Pswd_Filename%,%g_key%,Userid
		if  ((l_pswd = "ERROR") or (l_pswd = "" ))  {
			MsgBox Password has NOT been created before.
			Gui, Destroy
			goto begin
		}
				
			
		MsgBox, 4,,Sure you want to Overwrite Description and Userid?
		IfMsgBox,  No 
			goto begin
				
				
		InputBox, g_Description, Enter new description, Enter new description., , 200, 150,,,,,%g_Description% %l_decp%
		if  ErrorLevel
			MsgBox, CANCEL the description update
		else 
			f_Write_Description()					
			
		InputBox, g_Userid, Enter new Userid, Enter new userid., , 200, 150,,,,,%g_Userid% %l_userid%
		if  ErrorLevel
			MsgBox, CANCEL the Userid update
		else 
			f_Write_Userid()					
			
		MsgBox  Password %g_Key% updated!		
			
		Gui, Destroy
		Exit
	}

	/* 	
		===============================================================
					Remove Password
		===============================================================
	*/
	if  (g_Function = FUNTION_REMOVE) {
		
		if (f_Verfiy_Master() = -1)
			goto begin
		
		IniRead, pswd,  %g_Pswd_Filename%, %g_Key%
		if (pswd ="") {
			MsgBox Password %g_key% hasn't been created before.
			exit
		}
			
		MsgBox, 4,,Sure you want to remove password? All the relate information will be removed.
		IfMsgBox,  No 	
			exit
			IniDelete, %g_Pswd_Filename%, %g_Key%
		MsgBox  Password %g_Key% removed!					
		Gui, Destroy
		Exit
	}
	
	/* 	
		===============================================================
					View Email
		===============================================================
	*/

	if  (g_Function = FUNTION_VIEWEMAIL) {
		
		IniRead, l_Read_Email, %MASTER_CONFIG_FILE%, Password, Email
		
		If ((l_Read_Email<>"")and (l_Read_Email <>"ERROR")) 
			MsgBox Email Address is %l_Read_Email%					
		else 
			MsgBox Email hasn't  been set. Use Set Email to setup first.			
		Gui, Destroy
		Exit
	}
		
	/* 	
		===============================================================
					Set Email
		===============================================================
	*/

	if  (g_Function = FUNTION_SETEMAIL) {
		
	;	if (f_Verfiy_Master() = -1)
	;		goto begin
			
		IniRead, l_Read_Email, %MASTER_CONFIG_FILE%, Password, Email
		If ((l_Read_Email<>"")and (l_Read_Email <>"ERROR")) {
			MsgBox, 4, , Email Address already exist, Continue will overwrite old one.
			
			IfMsgBox, No
				goto begin
		}	
		
		InputBox, g_email, Enter email, Enter email address , , 150, 150
			
		IniWrite, %g_email%, %MASTER_CONFIG_FILE%, Password, Email
		MsgBox email address created. In order to receive email, config your office outlook account first. GMAIL, QQ is recommended.  check readme for detail					
		Gui, Destroy
		Exit
	}
		
		
	/* 	
		===============================================================
					Set Master Password
		===============================================================
	*/
	
	
	if  (g_Function = FUNTION_SETMASTER) {
								
		
			IniRead, l_Read_Master, %MASTER_CONFIG_FILE%, Password, Master
			
			l_Read_Master := Crypt.Encrypt.StrDecrypt(l_Read_Master,PUBLIC,5,1) 
			
			If ((l_Read_Master<>"")and (l_Read_Master <>"ERROR") and (l_Read_Master <> 0) ) {
				MsgBox, 4, , Master Password already exist, Continue will overwrite old one.
				IfMsgBox, No 
					goto begin
			}	
			
			MsgBox  For safety, Backup your password file before change master password!
			InputBox, l_OLD_Master, Verfiy Old, Enter Old Master password., Hide, 150, 150
			;~ verfiy password
			if (l_Read_Master <>l_OLD_Master){
				MsgBox Master Password mismatch.						
				goto begin
			}
			
			InputBox, l_New_Master, Enter New Password, Enter New Master password.,Hide , 150, 150
			
			
;  		master password can NOT be null.
 			If (l_New_Master = "" ){
  				MsgBox master password can't be null.
  				goto begin
  			}
 
			
			
			InputBox, l_New_Master_again, Confirm, Enter New Master password  again.,Hide , 150, 150
			if (l_New_Master_again<> l_New_Master){
				MsgBox New password mismatch. Try again.				
				goto begin
			}
			
			l_Masterpswd_enc:= Crypt.Encrypt.StrEncrypt(l_New_Master,PUBLIC,5,1) 					
				
			if ((l_Masterpswd_enc <>"") and (l_Masterpswd_enc <>0))  {
				i = 0	
				;~ decrp & encrp all the Password with the new Master password				
				Loop , %MAX_PASSWORD%{
					i++
					IniRead, l_pswd_enc,%g_Pswd_Filename%,%i%,Password																	
					IniRead, l_prevpswd_enc,%g_Pswd_Filename%,%i%,Prev_password																
					
					if ((l_pswd_enc = "")or(l_pswd_enc = "ERROR"))  
						continue 	 ; password not found											
											
					l_pswd_dec :=  Crypt.Encrypt.StrDecrypt(l_pswd_enc,l_Read_Master,5,1)							
					l_pswd_enc:= Crypt.Encrypt.StrEncrypt(l_pswd_dec,l_New_Master,5,1) 								  		
					if ((l_pswd_enc = 0) or (l_pswd_enc = "") or (l_pswd_enc = "ERROR")) {
						MsgBox encrpt Password failed, find your backup file or contact 23465446@qq.com for help.
						goto begin
					}
												
					IniWrite, %l_pswd_enc%, %g_Pswd_Filename%,%i%, Password													
					
					;~ update prev Password
					IniRead, l_prevpswd_enc,%g_Pswd_Filename%,%i%,Prev_password																
					
					if ((l_prevpswd_enc = "")or(l_prevpswd_enc = "ERROR"))  
						continue 	 ; password not found											
					
					l_prevpswd_dec :=  Crypt.Encrypt.StrDecrypt(l_prevpswd_enc,l_Read_Master,5,1)	
					l_prevpswd_enc:= Crypt.Encrypt.StrEncrypt(l_prevpswd_dec,l_New_Master,5,1) 		
					if ((l_prevpswd_enc = 0) or (l_prevpswd_enc = "") or (l_prevpswd_enc = "ERROR")) {
						MsgBox encrpt previous Password failed during set master password
						goto begin
					}
																		
					IniWrite, %l_prevpswd_enc%, %g_Pswd_Filename%,%i%, Prev_password		
				}
				
				IniWrite, %l_Masterpswd_enc%, %MASTER_CONFIG_FILE%, Password, Master
				MsgBox New Master Password Created, Don't Forget!
			}

	
		Gui, Destroy
		Exit
	}
	
	/* 	
		===============================================================
					Send Email
		===============================================================
	*/

	if  (g_Function = FUNTION_SENDEMAIL) {
		
		;~ Fill the mail
		Subject 	= Backup Pin File
		Body 		=  Hi  `n    You specify send mail to backup your pin file. The file has already been encrpted by 128 bits AES.`n`n`n
		
		
		
		InputBox, l_mail_memo, Enter your comments, Enter your comment for this backup( can be blank) ., 30, 300	
		
		Body = %Body%%l_mail_memo%
		
		Body = %Body%`n`n%A_NOW%
		IniRead, l_Master, %MASTER_CONFIG_FILE%, Password, Master			
		l_Master := Crypt.Encrypt.StrDecrypt(l_Read_Master,PUBLIC,5,1) 	
		
		Crypt.Encrypt.FileEncrypt(g_pswd_filename,ENCRYPT_FILE,l_Master,5,1)					
		
		f_Send_mail(Subject, Body)
		Gui, Destroy
		Exit
	}

	if  (g_Function = FUNTION_DECRYPTFILE) {
		
		;~ decrypt file
		MsgBox, 4, , Specify your encrypted password file. 		
		IfMsgBox, No   			; don't continue
			goto begin
			;~ specify the password fie
		
		FileSelectFile, Pswdenc_Filename, 3			
		if  Pswdenc_Filename=		
		{	
			MsgBox, 4,, password filename not specified, try it now?		; one more chanace to say YES
			IfMsgBox, No   
				goto begin
			FileSelectFile, Pswdenc_Filename, 3	
			if  Pswdenc_Filename = 
				goto begin
		}
		
				MsgBox, 4, , Specify your encrypted password file. 		
		IfMsgBox, No   			; don't continue
			goto begin
			;~ specify the password fie
		
		MsgBox, 4, , Specify your Decrypted password file. 		
		IfMsgBox, No   			; don't continue
			goto begin
			;~ specify the password fie
		
		FileSelectFile, Pswddec_Filename, S24			
		if  Pswddec_Filename=		
		{	
			MsgBox, 4,, Decrypt password filename not specified, try it now?		; one more chanace to say YES
			IfMsgBox, No   
				goto begin
			FileSelectFile, Pswddec_Filename, 	S24
			if  Pswddec_Filename = 
				goto begin
		}
		
		IniRead, l_Master, %MASTER_CONFIG_FILE%, Password, Master			
		l_Master := Crypt.Encrypt.StrDecrypt(l_Read_Master,PUBLIC,5,1) 	
		
		if (Crypt.Encrypt.FileDecrypt(Pswdenc_Filename,Pswddec_Filename,l_Master,5,1)	<= 0) 
			MsgBox decrypt failed.
		
	
		Gui, Destroy
		Exit
	}
	
	/* 	
		===============================================================
					 Set password
		===============================================================
	*/
	if  (g_Function = FUNTION_SET) {		
		MsgBox  you are going to SET your own password with no restriction. Use CREATE if you need a random password
		if 	 (g_Interval = "")		{
			MsgBox select the interval before set password
			Gui, Destroy
			goto begin
		}		
		f_set_password() 			; create password
		Gui, Destroy
		Exit
	}	
/*
ButtonHelp:
	f_display_help()
	goto begin
Exit
*/

Buttondhcp:				
		
	RunWait  netsh interface ip set dns Wireless source=static addr=9.0.148.50		
	dns = att
	Gui, Destroy
	Exit
	

Buttonatt:			
	
		
	RunWait netsh interface ip set dns Wireless source=dhcp
	dns = dhcp
	Gui, Destroy
	Exit
		
		
	

ButtonView:
	IniRead, g_Pswd_Filename, %MASTER_CONFIG_FILE%, Password, Directroy
	
	IniRead, l_Read_Email, %MASTER_CONFIG_FILE%, Password, Email
		

	MsgBox 	Password directory  --- %g_Pswd_Filename%          `n                                        
		   ,Email Address  -------- %l_Read_Email%			
	
	Gui, Destroy
	Exit	
	goto begin    

ButtonSet:
		FileSelectFile, g_Pswd_Filename, ,%g_Pswd_Filename%
		if  (g_Pswd_Filename = 	){
			MsgBox You have cancel the repick password 
			Exit
		}
			
	
	;~ write password file name to config file	
	IniWrite, %g_Pswd_Filename%, %MASTER_CONFIG_FILE%, Password, Directroy
	Gui, Destroy
	Exit	
	goto begin 


ButtonExit:
	ExitApp
Exit


GuiClose:
ButtonMin:
GuiEscape:
	;~ MsgBox Use Win+P to invoke this panel again.
	Gui, Destroy
	Exit
	
	
MainPanel:
	goto begin
	exit 

  
 About:
	MsgBox 	Password Manager V1.0 `n   	Author: Clark Hu     `n    23465446@qq.com 
	Exit

Help:
	f_display_help()
	goto begin
Exit


Exit:
	ExitApp