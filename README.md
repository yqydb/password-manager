#password manager
		
Password Manager Manual 
                                         Clark Hu
	
Contents

Highlight	3
Random password with specify rule	3
Safe to sue	3
Hotkey driven	3
Expiration remind	3
Easy to backup	3
Password Manager is portable	3
SAFE to use	4
Master Password	4
Data encrypt	4
HOW to use	4
run Password Manager	4
Enter master password.	5
Create your first password	5
1. Fill the Description, USERID	6
2. Select a rule best match for your password.	6
3. Select an interval best match for your password.	6
4. red char M S E indicates	7
All functions to handle your password	7
Set Password	7
Refresh Password	7
View Password	7
View Last Password	7
Update Password	7
Remove Password	8
Password Manager also supply some misc functions	8
View email	8
Set email	8
Set master	8
Send password file	8
Decrypt file	8
TIPS to use	8
Group similar usage password	8
Interval is important for expiration remind	9
Password manager will keep your last used password.	9
You are dead if you forget your master password.	9
Backup your password file.	9
Shortcut can be used for Password Manager.	9
















This tool is used to help you to maintain your password with various very handy functions.

Highlight 	
It’s very easy to use Password manager after going through this document. Just follow the step and you will familiar with it.
Flexible password rule
	Provide most common password rules, strong password will be auto generated for you
	If not, you can set your own password, and Password Manger will also help you to encrypt, save it.
Safe to sue
	Very safe to use comparing written your password to an excel or a piece of paper  
	Master password is used to verify authority 
	Powerful encrypt algorithm is used for any sensitive data.
	Detail can be found from next session
Hotkey driven
	Use HOTKEY Win+P to invoke the main panel. 
	After setup your password, enter hotkey (Win+Fn, n=1~8), and corresponding password (1~8) will auto typed on your active window fields.
Expiration remind 
	After picked up a proper interval for your password, Password Manager will remind you 5 days before expiration. 
Easy to backup
	All password data can be back up via email as long as you have an available Microsoft Office outlook mail client and account. 
	Once you set a backup directory, it will automatically backup your data file when you add/remove/refresh a password, and write a log record to help to recognize why the backup  happened and the result. 

Password Manager is portable 
	You can use it with all your password information on any Windows XP, Windows7, all your need is the password file as well as this software

Other usage 
In fact, it’s not limited to be used only for password, you even can use SET to set some frequently used strings, sentence, e.g. your intranet ID, a specific IP address. 

And most important, it’s easy to update, any smart ideas can be added into it later on whenever I have time.

SAFE to use
Master Password
	Master password is setup at first you open Password Manager by yourself.
	You will be asked to enter master password whenever you’re going to “touch” password 
	No restriction for the rule of master password. I suggest you pick up a good one, as this is one and only one gate to protect your password information in this software.
	Master password can be changed at any time, and can never be changed.
Data encrypt 
	Any sensitive data will be encrypted.  
	AES 128 bit is used to encrypt, you will find how save your password will be from Google “AES 128”  
	If you choose email backup, whole password file will be encrypted first, and send to destination. You have to provide your master password to decrypt it later.    

HOW to use
Run Password Manager
You will be asked to specify a password file for the first time
 

The password file can be non-existing if you haven’t had one before, or existing file if you have move your password file to other PC or reinstall your system
Make sure your password file is safe and can be backed up easily.

Enter master password.
 

If you have already set master password, you will be prompt to enter and continue. Wrong one will cause Password Manager exit.
 

Master password can be any character, any length, and no expiration days. 
For safety reason, it should be complicate enough and change it regularly.      

Create your first password
After above 2 steps, you can use Win+P to invoke main panel:
 

From the main panel you can see, 8 password slots (correspond with Win+ F1,F2..F8 to output) is provided, select one before handle it. 

Now Let’s create a new password!
 
1. Fill the Description, USERID 
	Both are optional. 
	It will be very useful if you enter meaningful some words in it when     you have to change the expiring password! 
	Description can be account, website or application name. 

2. Select a rule best match for your password. 
The available rules are:
       

6 char  Aa1***	good for a 6 character password;
8 char  Aa1*****	Most common password rule, can satisfy majority requirement.
12 char Aa1*********  	Stronger in length, use this rule if a 12 length password is allowed. Usually used for website password!
12 char Aa1@********  	strongest password rule by using any special character: !,@,#,$,%,&,*,(,)	 	

3. Select an interval best match for your password. 
	 Available interval can be 1 month, 2 months, 3 months, 1 year, never;
	 Using never if password is expected as never expire. 
	 This only effect the reminder function, you still can refresh password even your interval is set to never.

4. Red char M S E indicates
 
	 	M	-	Password has been used Multiple times.
		S	-	Password has been created and never changed before 
		E	-      Password is going to Expire     
		X	-      Password is eXpired.
	    Na    -      Not used.
All functions to handle your password
 
Except for Create, you can:
Set Password
	 Manually set your password with no rule restriction. (Not recommended)
	 you can’t refresh a manually set password

Refresh Password
Refresh your password.

View Password
	 View all your password information, 
	 including when password has been created, the rule, interval, description, userid.

View Last Password
View last password, and directly copy to your clipboard.

Update Password
	 Update your password information. 
	 You can update Description, Userid. 
	 You can’t update rule, interval after the password has been created. Remove and re-create it.

Remove Password
Remove all the specific password information, and make it available for reuse.

Password Manager also supply some misc functions
View email
View your email address previous set.
Set email
	 Set a new email address. 
	 Before you can use mail backup, a Microsoft office outlook client as well as a well-config mail account are required  
	 Config a GMAIL account, click here. Config a QQ mail account, click here. 
Set master
Set or overwrite master password
Send password file
 
	 Send password file to your destination mailbox. 
	 The file has been encrypted before attached to mail
Decrypt file
	 After you download your password file from email, you need decrypt it first before you can use.
	 Decrypt password is your master password

	
TIPS to use
Group similar usage password
	Use only one password with the same rule and change interval 
	Good experience
	 Intranet and notes password can share the same password with interval 3 month, password rule 2(Aa1*****)   
	 Use only one password all TSO with interval 1 month, password rule 2 (Aa1*****). 
	 Use interval never, password rule (Aa1@********) for your website password.
	Random generated password should fulfill most of the password rules. 

Interval is important for expiration remind 
Specify correct interval and password manager will remind you to refresh password before it expires. 

Expiration date will be checked during startup and invoke main panel.
 

Password manager will keep your last used password. 
This is very useful while your password is about to expire and you need to change a new one.

You are dead if you forget your master password.
It’s your responsibility to remember this most important key. 

Backup your password file.
1). Backup your password file to a safety place.
2). set backup directory will help you automatically backup when necessary 
3). It’s really convenience to use mail backup. Set your outlook account now! 

Shortcut can be used for Password Manager.
1). type first letter of the function, 
  e.g  C for Create, R for Remove & Refresh, and so on.
2). Alt+n can be used to quickly select password n

Small Tray can be handy for mouse click
 
1). right click icon tray to launch main panel or help document
2). Double click icon to launch main panel. 



Change History 
  
Version 	Author	Date	 Description
V1.0 	Clark Hu	05/10/13	First init 
V1.1	Clark Hu	08/10/13	Add calendar tool to convert Julian date
V1.2	Clark Hu	05/10/14	Add DHCP and maunlly DSN switch for AT&T.
V1.3	Clark Hu	07/10/14	Add button to set and view configuration file
V1.5 	Clark Hu	09/01/14	Remove DHCP function;
V2.1    Clark Hu        04/28/14        add ftp submit function. read the config from config file, and change password from mainframe
V2.2    Clark Hu        09/18/15        remove dhcp switch function, add password history
                                        add backup remind after change password      

adjust several bugs;
add backup remind after change password

Any comments are welcome! 23465446@qq.com.

