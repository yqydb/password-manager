#Include Crypt.ahk
#Include CryptConst.ahk
#Include CryptFoos.ahk
#Persistent
#SingleInstance

;Crtl+Win+P is to invoke maint panel

;Read config.ini

;~ set the max number of password allow to use.
Global  g_Pswd_Filename,g_Function,g_Key,g_Description, g_Rule, g_Interval, g_Description, g_Userid, g_email, g_Masterpswd,g_update,g_update_Interval, g_update_Description,g_inter_choose,g_used_1,g_used_2,g_used_3,g_used_4,g_used_5,g_used_6,g_used_7,g_used_8



Global  MAX_PASSWORD,NOTIFY_DAYS,LAST_NOTIFY_DAYS , FUNTION_CREATE, FUNTION_REFRESH,FUNTION_VIEW,FUNTION_VIEWLAST,FUNTION_UPDATE, FUNTION_REMOVE,FUNTION_VIEWEMAIL,FUNTION_SETEMAIL,FUNTION_VIEWMASTER,FUNTION_SETMASTER

MAX_PASSWORD 			= 8
NOTIFY_DAYS  			= 5
LAST_NOTIFY_DAYS   		= 1


FUNTION_CREATE      	= 1
FUNTION_SET				= 2
FUNTION_REFRESH    		= 3
FUNTION_VIEW    		= 4
FUNTION_VIEWLAST		= 5
FUNTION_UPDATE  		= 6
FUNTION_REMOVE			= 7
FUNTION_VIEWEMAIL		= 8
FUNTION_SETEMAIL		= 9
FUNTION_SETMASTER		= 10
FUNTION_SENDEMAIL		= 11
FUNTION_DECRYPTFILE	= 12

PUBLIC                      := "PASSWORD MASTER"
SEND_FILE					:= "pswd.enc"yyNfxH3J

Global  MASTER_CONFIG_FILE,  PUBLIC, MASTER_CONFIG_FILE_CR,ENCRYPT_FILE, DNS

;reset dns to dhcp
; RunWait netsh interface ip set dns Wireless source=dhcp
; dns = dhcp
MASTER_CONFIG_FILE = %A_ScriptDir%\AHKpswd.cfg

ENCRYPT_FILE   =  %A_ScriptDir%\pswd.enc





f_Config_Read()
IniWrite, NO, %MASTER_CONFIG_FILE%, Password, Verify
f_Verfiy_Master()

;MsgBox Press hotkey Win+P to invoke main panel Win+Fn to output Password


menu, tray, NoStandard
menu, tray, add, MainPanel
menu, tray, add, Help
menu, tray, add, About
menu, tray, add, Exit 
menu, tray, Default, MainPanel

#p::

Begin:	
Gui, Destroy
While true {
	f_Config_Read()
	f_Check_expire()
	f_Create_MasterPanel()	
	return
}

/*
+#F1::
	f_display_help()
	exit 
*/


#F1::
	g_key := 1
	f_Verify_verified()
	if  f_Read_Password(pswd) = -1
		exit 
	f_Check_expire()
	SendRaw %pswd%	
	exit 

#F2::
	g_key := 2
	f_Verify_verified()
	if f_Read_Password(pswd) = -1
		exit	
	f_Check_expire()
	SendRaw %pswd%	
	exit 

#F3::
	g_key := 3
	f_Verify_verified()
	if f_Read_Password(pswd) = -1
		exit
	f_Check_expire()
	SendRaw %pswd%
	exit 
	
#F4::
	g_key := 4
	f_Verify_verified()
	if f_Read_Password(pswd) = -1
		exit
	f_Check_expire()
	SendRaw %pswd%
	exit 
	
#F5::
	g_key := 5
	f_Verify_verified()
	if f_Read_Password(pswd) = -1
		exit
	f_Check_expire()
	SendRaw %pswd%
	exit 
	
#F6::
	g_key := 6
	f_Verify_verified()
	if f_Read_Password(pswd) = -1
		exit
	f_Check_expire()
	SendRaw %pswd%
	exit 

#F7::
	g_key := 7
	f_Verify_verified()
	if f_Read_Password(pswd) = -1
		exit
	f_Check_expire()
	SendRaw %pswd%
	exit 
	
#F8::
	g_key := 8
	f_Verify_verified()
	if f_Read_Password(pswd) = -1
		exit
	f_Check_expire()
	SendRaw %pswd%
	exit 

#j::	
	run "%A_ScriptDir%\.exe"
	Exit	
	

#h::	
	f_display_help()
	Exit


f_Config_Read()  
{
	;~ check if config file exist, Both the Name and Directory is fixed!
	IfNotExist  %MASTER_CONFIG_FILE% 
	{
	;~  First specify password filename, then create AhKpswd.cfg
		MsgBox, 4,, 如果这是你第一次使用，请认真阅读帮助文件。
		IfMsgBox, Yes
			f_display_help()	
			
		
		MsgBox, 4, , 指定你保存密码文件的位置, 或者选择已存在密码文件
		IfMsgBox, No   			; no password file, exit 
			ExitApp
			;~ specify the password fie
		
		
		FileSelectFile, g_Pswd_Filename, S8			
		if  g_Pswd_Filename=		
		{	
			MsgBox, 4,, 还未指定密码文件，重试吗？ 
			IfMsgBox, No   
				ExitApp
			FileSelectFile, g_Pswd_Filename, S8
			if  g_Pswd_Filename = 
				ExitApp
		}
		
		
		
		IniWrite, %g_pswd_filename%, %MASTER_CONFIG_FILE%, Password, Directroy
		IniWrite, %A_Now%, %g_Pswd_Filename%,Init, Time	
		IniWrite, %g_pswd_filename%, %g_Pswd_Filename%,Init, Directory	
	;MsgBox %g_pswd_filename%
		return
		
					
	}
	
	IniRead, g_Pswd_Filename, %MASTER_CONFIG_FILE%, Password, Directroy
	
	;~ IniRead,Master_Pswd,%MASTER_CONFIG_FILE%,Master,Security,Master
	;~ if  Master_Pswd =""{			
		;~ MsgBox,4,, You haven't 
	
	;~ set to NOverfiy each time start the tool	
	return  
}


f_display_help(){
x1 = 
(
用户指南
    
* 第一次使用请指定一个位置存放密码文件，请确定密码文件安全并定期备份。

   
*  设置主密码
    主密码文件没有字符，长度，期限的限制。
	您所有设置的密码都会用主密码来进行加密，请避免泄漏，同时牢记，这是你唯一可以使用密码文件的方法，没有主密码文件，将不能使用之前设置的任何密码。	
	
*  快捷键驱动
	本软件完全使用快捷键操作，请牢记以下快捷键
	Win+P    调用主面板，完成密码的维护功能
	WIn+Fn    Fn是F1~F8任一功能键，输出对应1~8组密码文件	
 )

x2  = 
(
 I. 创建密码
 你可以最多8组密码，对应的F1~F8 8组快捷键
 
* 每组密码可以添加描述，用户名 （可选）
	描述可以是任何有意义的语句，方便将来修改密码，或者回忆改密码的用途。
	

* 设定密码规则
  软件提供了以下密码规则供参考
  6 char  Aa1***		长度为6，密码包含大写，小写，数字 
  8 char  Aa1*****		长度为8，密码包含大写，小写，数字
  12 char Aa1*********  长度为12，密码包含大写，小写，数字
  12 char Aa1@********  长度为12，密码包含大写，小写，数字，以及特殊字符! @ # $ & * ( )	 
	
	可以根据实际需要选择密码规则，在创建每个密码时分别指定。

* 选择密码有效期
  可选有效期包括1个月，2个月，3个月，1年，永久。   
   注意：该有效期设置仅用于提醒功能，不会自动修改您的密码，您也可以在密码过期后使用该密码，如果不需要修改密码，请选择永久有效。

* 字符提醒
    每个使用过的密码前有红色字母，
	M	-	多次使用，更新过密码
	S	-	设置了密码，并且没有更新过
	E	-    该密码即将过期
	X	-    该密码已经过期
)

x3  =      
(
II. 设定密码  
	该软件提供了手动设定密码功能，可以指定任意长度，字符的密码
	如非必要，建议通过指定密码规则创建密码。
	你也可以使用该功能设定常用语句，通过快捷键快输输出。
    

III. 刷新密码
	刷新指定密码，新密码将使用同样密码规则和有效期。

IV. 查看密码信息
	查看指定密码的信息，包括 密码创建的日期，密码规则，过期时间，以及之前填写的用户名。
	
V. 查看上次密码
	使用该功能将更新前的密码拷贝到粘帖板，使用Ctrl+V输出
	
VI. 更新密码
	更新密码信息，包括密码描述和用户名，
	注意你不能更新密码规则，有效期。只能通过删除，重新创建密码。
	

VII. 删除密码
	删除指定密码的所有信息	
)

X4 = 
(
	其他功能
VIII. 查看邮箱
	查看之前设置的邮箱地址
	

IX. 设置email地址
	该软件提供了邮箱备份。将密码文件加密发送到指定邮箱。建议使用QQ邮箱，或gmail。
	使用该功能前，需要配置邮箱客户端，可以使用foxmail或者office outlook。配置方法在邮箱网站有介绍。
	

X. 更新主密码
	更新主密码。再一次，主密码非常重要，请牢记并保管好。

XI. 发送密码文件 
 
	将密码文件通过附件的方式发送到指定邮箱。
	请注意密码文件已经通过AES 128bit 加密，保证安全。

XII.解密文件
	该功能用于恢复从邮箱下载的加密后的密码文件，需要您的主密码来进行解密	
)


X5 =
(
	注意
	为了方便日常密码管理，本人利用空闲时间开发该软件.
	测试并实际使用超过2年未遇到任何问题，但是无法保证所有用户都不会遇到问题，
	使用者最好定期对密码文件备份，并妥善保管文件。 
	如有意外发生,或者密码泄露。  用户需承担任何责任。 继续使用软件表示用户同意以上论述。
	更详细介绍请阅读附带的“说明文件.doc”
)

MsgBox  ,1,,%x1%,300

IfMsgBox OK
	MsgBox  ,1,,%x2%,300

IfMsgBox OK
	MsgBox  ,1,,%x3%,300

IfMsgBox OK
	MsgBox  ,,,%x4%,300

IfMsgBox OK
	MsgBox  ,4,,%x5%,300

IfMsgBox No
	ExitApp

return
}



f_Verfiy_Master(){
	IniRead, l_Read_Master, %MASTER_CONFIG_FILE%, Password, Master
	
	; if ((l_Read_Master="")or (l_Read_Master = "ERROR")or(l_Read_Master = 0)) 
	;	InputBox, l_Userid, Create User ID,  _=========== Create User ID =========_`n `nNOTE: `n Remember your ID, it's required while logon   , , 400, 300
	
	IniRead, l_Read_Master, %MASTER_CONFIG_FILE%, Password, Master
	if ((l_Read_Master="")or (l_Read_Master = "ERROR")or(l_Read_Master = 0)) {
		InputBox, l_Master, 输入主密码,  _=========== 输入主密码 =========_`n `n 注意: `n 1. 在下面输入主密码.`n 2. 请牢记输入的主密码，该软件所有功能都需要主密码，并妥善保管 `n 3. 如果您之前使用过该软件，并试图使用之前的密码文件，请输入和密码文件匹配的主密码，否则无法正常使用 `n 4. 通过该主密码文件，您的密码文件可以在任一台windows 系统使用.   ,HIDE , 400, 300
		
		;InputBox, l_Master, Enter User ID & Master Password,  _=========== Enter  master Password =========_`n `nNOTE: `n 1. Enter your New Master 
		;password.`n 2. Don't use arbitrary one`, it's the key to access all your Password`n 3. If you have set master Password before`, enter the same one `n 4.Your config file can be read and used on any PC with your correct Master Password.   ,HIDE , 400, 300
	
		if (l_Master="") {
			MsgBox 你必须提供一个主密码，软件退出。。。
			ExitApp
		}
		
		InputBox, l_Master_Sec, 再次输入，确认密码,再次输入，确认密码 , 200, 150
		if (l_Master_Sec<>l_Master){
			MsgBox 两次输入不匹配，退出。。。 
			ExitApp				
		}
		
		
		l_Masterpswd_enc:= Crypt.Encrypt.StrEncrypt(l_Master,PUBLIC,5,1) 	
		if  ((l_Masterpswd_enc ="")or (l_Masterpswd_enc= "ERROR")or(l_Masterpswd_enc = 0)) {
			MsgBox 加密主密码失败 请联系 qq 23465446. %l_Masterpswd_enc%
			ExitApp
		}
		IniWrite, %l_Masterpswd_enc%, %MASTER_CONFIG_FILE%, Password, Master
		MsgBox 主密码创建成功，请牢记并妥善保管
		
	}
	else {
		InputBox, l_Verify_Master,  输入,输入主密码, HIDE , 200, 150
		l_Masterpswd_enc:= Crypt.Encrypt.StrEncrypt(l_Verify_Master,PUBLIC,5,1) 	
		if (l_Masterpswd_enc<>l_Read_Master){
			MsgBox 两次输入不匹配
			if (g_Function ="")  ;First time to verify, exit directly
				ExitApp
			else 				;invoke primary panel again.
				return -1
		}
		IniWrite, YES, %MASTER_CONFIG_FILE%, Password, Verify	
	}	
	
}

f_Verify_verified()
{
	IniRead, verified, %MASTER_CONFIG_FILE%, Password, Verify
	if verified <> YES 
	{
		MsgBox You have to verify your master first!
		f_Verfiy_Master()	
	}
}


f_check_decrypt(str,caller)
{
	if (str="") {
		MsgBox 解密失败，请联系qq 23465446 %caller%
		exit 	
	}
		
}

f_Check_expire()
{
	
	i = 0
	Loop , %MAX_PASSWORD%{
		i++
		l_current_stamp := A_Now
		IniRead, l_Stamp,%g_Pswd_Filename%,%i%,Stamp
		IniRead, l_interval,%g_Pswd_Filename%,%i%,Interval
		
		if ((l_interval = "") or(l_interval =0) or (l_Interval = "ERROR"))
			continue 
		
		if  (l_Interval = 4)   		;  selection is 1 year
			l_Interval = 12
	
		if (l_Interval = 5) 		; never expire	
			l_Interval = 9999	
	
		
		if ((l_Stamp <>"") and (l_Stamp <>"ERROR")){
			EnvSub, l_current_stamp, %l_Stamp%, days
			
			l_interval *= 30 		
						
			if 	(l_interval - l_current_stamp<= NOTIFY_DAYS and l_interval - l_current_stamp>LAST_NOTIFY_DAYS)  			;~ Give notify 5 days before expire
				MsgBox 密码 %i%  即将过期， 请立刻更新密码！
			if  (l_interval - l_current_stamp < LAST_NOTIFY_DAYS ) 
				MsgBox 密码 %i% 已经过期，请立刻更新密码。
					;~ f_Send_mail()
			
		}				
	}
}

f_Check_Used(  )
{
	i = 1
	Loop , %MAX_PASSWORD%{
	
		IniRead, l_pswd,%g_Pswd_Filename%,%i%,Password
		IniRead, l_prev_pswd,%g_Pswd_Filename%,%i%,Prev_password
		
				l_current_stamp := A_Now
		IniRead, l_Stamp,%g_Pswd_Filename%,%i%,Stamp
		IniRead, l_interval,%g_Pswd_Filename%,%i%,Interval
		
		if ((l_interval = "") or(l_interval =0) or (l_Interval = "ERROR"))
			continue 
		
		if  (l_Interval = 4)   		;  selection is 1 year
			l_Interval = 12
	
		if (l_Interval = 5) 		; never expire	
			l_Interval = 9999	
	
		
		if ((l_Stamp <>"") and (l_Stamp <>"ERROR")){
			EnvSub, l_current_stamp, %l_Stamp%, days
			
			
			l_interval *= 30 	
			if ((l_pswd = "") or (l_pswd = "ERROR"))
				g_used_%i% = ""
			if ((l_prev_pswd = "") or (l_prev_pswd = "ERROR"))	
				g_used_%i% = S
			if  ((l_prev_pswd != "") and (l_prev_pswd != "ERROR") )
				g_used_%i% = M	
						
			if i = 5 
				MsgBox  diff is %l_interval% %l_current_stamp%   %NOTIFY_DAYS% %LAST_NOTIFY_DAYS%
			
			if 	(l_interval - l_current_stamp<= NOTIFY_DAYS and l_interval - l_current_stamp>LAST_NOTIFY_DAYS)  
				g_used_%i% = E
			if (l_interval - l_current_stamp < LAST_NOTIFY_DAYS ) 
				g_used_%i% = X
			

		}		
	i++		
	}
}

f_Create_MasterPanel( )
{	
	f_Check_Used( )
	Gui,1:Add, GroupBox,center w280 h245 , Password Master
	Gui,1: +MinimizeBox 
	Gui,1:Add, Text,x20 y25 cGray,&Function
	Gui,1:Font, w700 q5, Tahoma
	Gui,1:Add, DDL, AltSubmit x80 y25 w130 vg_function,  创建密码|设定密码|刷新密码|查看密码信息|复制上次密码|手动更新|删除密码|查看email|设置email|更新主密码|发送密码文件|解密	
	;Gui,1:Add, DDL, AltSubmit x80 y25 w130 vg_function,  Create|Set|Refresh|View|ViewLast|Update|Remove|View Email|Set Email|Set Master|Send password file|Decrypt File 
	Gui,1:Font,,		
		
	Gui,1:Font,  q5  w550 s7	
 	Gui,1:Add, Text,x30 y50 cRed  ,%g_used_1%
 	Gui,1:Add, Text,x30 y70 cRed ,%g_used_2%
  	Gui,1:Add, Text,x30 y90 cRed ,%g_used_3%
  	Gui,1:Add, Text,x30 y110 cRed,%g_used_4%
  	
  	Gui,1:Add, Text,x150 y50 cRed,%g_used_5%
  	Gui,1:Add, Text,x150 y70 cRed,%g_used_6%
  	Gui,1:Add, Text,x150 y90 cRed,%g_used_7%
  	Gui,1:Add, Text,x150 y110 cRed ,%g_used_8%
	Gui,1:Font,,		
	
		
	Gui,1:Add, Radio, x45 y50 vg_Key, 密码 &1
	Gui,1:Add, Radio, x45 y70, 密码 &2
	Gui,1:Add, Radio, x45 y90, 密码 &3
	Gui,1:Add, Radio, x45 y110, 密码 &4
	Gui,1:Add, Radio, x165 y50 , 密码 &5
	Gui,1:Add, Radio, x165 y70, 密码 &6
	Gui,1:Add, Radio, x165 y90, 密码 &7
	Gui,1:Add, Radio, x165 y110, 密码 &8
		

	Gui,1:Add, GroupBox, x15 y130 w270 h115, 
	Gui,1:Add, Text,x20 y145 cGray,&描述
	Gui,1:Add, Edit,x80  y145 r1 h15 w200  vg_Description,              
	Gui,1:Add, Text,x20 y170 cGray,&用户名
	Gui,1:Add, Edit,x80  y170 r1 h15 w200  vg_Userid,              
	Gui,1:Add, Text,x20 y195 cGray,&规则
	Gui,1:Add, DDL, AltSubmit x80 y195 w200 vg_Rule,  6 char  Aa1*** |8 char  Aa1***** |12 char  Aa1********* |12 char  Aa1@********
	Gui,1:Add, Text,x20 y220 cGray,&有效期
	Gui,1:Add, DDL, x80 y220 AltSubmit w200 vg_Interval %g_inter_choose%,  1 个月 |2 个月|3 个月|1 年|永不过期
/* 		
 * 	Gui,1:Add, Text,x20 y245 cGray,&Email
 * 	Gui,1:Add, Edit,x80  y245 r1 h15 w200  vg_Email,              
 * 		
 * 	Gui,1:Add, Text,x20 y270 cGray,&Master PSW
 * 	Gui,1:Add, Edit,x80  y270 r1 h15 w200 Password vg_Masterpswd, 
 */

		;~ Gui, Font,w700 , Arial
	Gui, Font, s8
	Gui,1:Add, Button,x10 y260,查看
	Gui,1:Add, Button,x50 y260 ,设置
	;Gui,1:Add, Button,x71 y260 ,Min
	;Gui,1:Add, Button,x107 y260 ,Help
	Gui,1:Add, Button,x100 y260 ,帮助	
	Gui,1:Add, Button,x140 y260 ,确定	
	
	
	Gui,1:Font,  q5  w550	
	Gui,1:Add, Text,x230 y255 ,Clark Hu
	Gui,1:Add, Text,x200 y270 ,23465446@qq.com
	Gui,1:Font,,		
	
	Gui 1:-Border
	Gui,1:Margin, 5, 10
	Gui,1:Show,AutoSize Center,Password master
	return	
}

f_create_UpdatePanel()
{	
	
	IniRead, l_decp,%g_Pswd_Filename%,%g_Key%,Description	
	IniRead, l_interval,%g_Pswd_Filename%,%g_Key%,Interval


	Gui,2:Add, GroupBox,center w220 h140 , Update Password %g_Key%						
	Gui,2:Add, Radio, x20 y25 vg_update, Description 
	Gui,2:Add, Radio, x20 y40, Interval
	Gui,2:Add, Text,x20 y70 cGray,&Description
	Gui,2:Add, Edit,x80  y70 r1 h15 w200  vg_update_Description,%l_decp%            				
	Gui,2:Add, Text,x20 y95 cGray,&Interval	
	
	Gui,2:Add, DDL, x80 y95 AltSubmit w200 vg_update_Interval  Choose%l_interval%,  1 个月 |2 个月 |3 个月 |1 年 | 永不过期


	;~ Gui, Font,w700 , Arial
	Gui,2:Add, Button,x20 y110 , Update
	Gui,2:Add, Button,x80 y110 , Cancel
	
	Gui 2:-Border
	;Gui,2:Margin, 5, 10
	Gui,2:Show,AutoSize Hide


	
	return	
}

f_set_password()
{
	
	IniRead, l_pswd,%g_Pswd_Filename%,%g_Key%,Password
	IniRead, l_decp,%g_Pswd_Filename%,%g_Key%,Description


	if  (((l_pswd = "ERROR") or (l_pswd = "" )) and ((l_decp= "ERROR") or (l_decp="" )))  {     	; password haven't create before
		
		
		
		
		
		InputBox, l_New_pswd, 输入新密码, 输入新密码 ,Hide , 150, 150
		if ( l_new_pswd = ""){
			MsgBox 新密码不能为空。
			return
		}
		
		InputBox, l_New_pswd_again, 确认新密码, 确认新密码, Hide, 150, 150
			;~ verfiy password
			if (l_New_pswd_again <>l_New_pswd){
				MsgBox 密码不匹配，请重试
				return
			}
		
		IniRead, Mpswd, %MASTER_CONFIG_FILE%, Password, Master		
		
		;~ msgbox  "master password"  %Mpswd%
		Mpswd_de := Crypt.Encrypt.StrDecrypt(Mpswd,PUBLIC,5,1)		
		
		l_new_pswd:= Crypt.Encrypt.StrEncrypt(l_New_pswd,Mpswd_de,5,1) ; encrypts string using AES_128 encryption and MD5 hash
		
		MsgBox 密码已成功设置，使用 Win+F%g_key% 输出		
		IniWrite, %l_new_pswd%, %g_Pswd_Filename%,%g_Key%, Password	
		IniWrite, %A_Now%, %g_Pswd_Filename%,%g_Key%, Stamp	
		
		; rule = M means manual set password!
		g_Rule = M
		
		f_Write_Description()  		
		f_Write_Userid()  		
		f_Write_Interval()
		f_Write_Rule()
	}
	else 
		MsgBox   使用 refresh来覆盖已存在的密码，或者	 `n       使用REMOVE 删除，并随后创建新密码。
	return
}

f_create_password()
{
	
	IniRead, l_pswd,%g_Pswd_Filename%,%g_Key%,Password
	IniRead, l_decp,%g_Pswd_Filename%,%g_Key%,Description


	if  (((l_pswd = "ERROR") or (l_pswd = "" )) and ((l_decp= "ERROR") or (l_decp="" )))  {     	; password haven't create before
		f_Write_Password()	
		f_Write_Description()  		
		f_Write_Userid()  
		f_Write_Interval()
		f_Write_Rule()
		f_Read_Password(l_new_pswd)
		MsgBox   密码 %g_Key%  是 %l_new_pswd%, 有效期选择是 %g_Interval%
	}
	else 
		MsgBox  使用 刷新 来覆盖已经存在密码， 或者 `n        删除密码再重建
		
	return
}

f_Write_Password(){
	
	f_Generate_Password(pswd)
	
	
	If  not FileExist(g_pswd_filename) {
		MsgBox  请指定文件 
		FileSelectFile, g_pswd_filename, S8
		
		if  (g_pswd_filename = "" ) {
			MsgBox, 你没有正确选择
			Exit
		}
		;~ save password file name to config file
		else	IniWrite, %g_pswd_filename%, %MASTER_CONFIG_FILE%, Password, Directroy
	}
	
	IniWrite, %pswd%, %g_Pswd_Filename%,%g_Key%, Password	
	IniWrite, %A_Now%, %g_Pswd_Filename%,%g_Key%, Stamp		
	;~ f_Send_mail( ) 	
 return  
}


f_Write_Description(){		
		
	if  ((g_Description != "ERROR") and (g_Description !=""))  {
		IniWrite, %g_Description%, %g_Pswd_Filename%, %g_Key%,  Description		
		;MsgBox Description for  password%g_Key%,Updated.	
	}	
	else  if(g_Description !="") {
		MsgBox 密码描述无法更新 %g_Description%  `n 请联系作者
		return -1
	}
		
	return
}

f_Write_Userid(){		
		
	if  ((g_Userid != "ERROR") and (g_Userid !=""))  {
		IniWrite, %g_Userid%, %g_Pswd_Filename%, %g_Key%,  Userid		
		;MsgBox Userid for  password%g_Key%,Updated.	
	}	
/* 	else  {
 * 		MsgBox USERID haven't been updated.  %g_Userid%
 * 		return -1
 * 	}
 */
		
	return
}

f_Write_Rule(){
	
	if ((g_Rule = "") or(g_Rule = "ERROR")){
		MsgBox 密码规则无法更新， %g_Rule% `n 请联系作者
		Return -1
	}
	IniWrite, %g_Rule%, %g_Pswd_Filename%,%g_Key%, Rule
}

f_Write_Interval(){
	
	
	l_Interval := g_Interval	
	
	if  (l_Interval not in 1,2,3,4,5)
	{
		MsgBox 密码有效期无法更新，使用的是 %l_interval%  `n 请联系作者 
		return
	}
	
	
	
	IniWrite, %l_Interval%, %g_Pswd_Filename%, %g_Key%,  Interval
		
	if  ((l_Interval = "ERROR") or (l_Interval ="")) 		
		MsgBox 密码有效期无法更新，使用的是 %l_interval%  `n 请联系作者 
	
	return
}

f_Read_Password(ByRef pswd){
	
	IniRead, encpswd,%g_Pswd_Filename%,%g_Key%,Password
	
	if ((encpswd = "")or(encpswd = "ERROR"))  {
		MsgBox 密码还未使用
		return -1
	}
	
	IniRead, Mpswd, %MASTER_CONFIG_FILE%, Password, Master
	; decrypts the string using previously generated hash,AES_128 and MD5
	Mpswd := Crypt.Encrypt.StrDecrypt(Mpswd,PUBLIC,5,1) 					
	pswd := Crypt.Encrypt.StrDecrypt(encpswd,Mpswd,5,1)				  		
	return
}

f_View_Password(){
	
	IniRead, l_pswd,  %g_Pswd_Filename%, %g_Key%
	;~ Gui,Font, w1000 s18 q1, Tahoma
	if (l_pswd ="" ) 
		MsgBox 密码 %g_key% 未被使用
	else 
		MsgBox  %l_pswd%
}				

;~ Read previous password
f_Read_PrePassword(){
	
	IniRead, encpswd,  %g_Pswd_Filename%, %g_Key%,Prev_password
	
	
	if ((encpswd = "")or(encpswd = "ERROR"))  {
		MsgBox 密码没有被更新过
		return -1
	}
	
	
	IniRead, Mpswd, %MASTER_CONFIG_FILE%, Password, Master
	
	; decrypts the string using previously generated hash,AES_128 and MD5
	Mpswd := Crypt.Encrypt.StrDecrypt(Mpswd,PUBLIC,5,1) 						
	f_check_decrypt(Mpswd,"read pre password decrypt Master password ")	
	
	pswd := Crypt.Encrypt.StrDecrypt(encpswd,Mpswd,5,1)				  
	f_check_decrypt(pswd,"decrypt password" %g_key%)	
	
			
	MsgBox 旧密码已被复制，请使用Ctrl+V 输出
	Clipboard  := pswd
	return
}

f_Remove_Password(){
	
	MsgBox, 4,,确定删除密码？所有信息将被删除并且无法恢复！！
	
	IfMsgBox,  No 	
	return
	
	IniRead, pswd,  %g_Pswd_Filename%, %g_Key%
	if (pswd ="") {
		MsgBox 密码 %g_key% 没有使用
		return
	}
			
	IniDelete, %g_Pswd_Filename%, %g_Key%
	
	MsgBox  密码 %g_Key% 删除成功
	return
}	

f_Generate_Password(ByRef Pswd){
	;~ Define all the character
	Char_1:="A"
	Char_2:="B"
	Char_3:="C"
	Char_4:="D"
	Char_5:="E"
	Char_6:="F"
	Char_7:="G"
	Char_8:="H"
	Char_9:="I"
	Char_10:="J"
	Char_11:="K"
	Char_12:="A"
	Char_13:="M"
	Char_14:="N"
	Char_15:="O"
	Char_16:="P"
	Char_17:="Q"
	Char_18:="R"
	Char_19:="S"
	Char_20:="T"
	Char_21:="U"
	Char_22:="V"
	Char_23:="W"
	Char_24:="X"
	Char_25:="Y"
	Char_26:="Z"
	Char_27:="a"
	Char_28:="b"
	Char_29:="c"
	Char_30:="d"
	Char_31:="e"
	Char_32:="f"
	Char_33:="g"
	Char_34:="h"
	Char_35:="i"
	Char_36:="j"
	Char_37:="k"
	Char_38:="m"
	Char_39:="m"
	Char_40:="n"
	Char_41:="o"
	Char_42:="p"
	Char_43:="q"
	Char_44:="r"
	Char_45:="s"
	Char_46:="t"
	Char_47:="u"
	Char_48:="v"
	Char_49:="w"
	Char_50:="x"
	Char_51:="y"
	Char_52:="z"
	Char_53:="0"
	Char_54:="1"
	Char_55:="2"
	Char_56:="3"
	Char_57:="4"
	Char_58:="5"
	Char_59:="6"
	Char_60:="7"
	Char_61:="8"
	Char_62:="9"
	Char_63:="!"
	Char_64:="@"
	Char_65:="#"
	Char_66:="$"
	Char_67:="%"
	Char_68:="&"
	Char_69:="*"
	Char_70:="("
	Char_71:=")"
	

	;~ Read config to check if rule already exist 
	IniRead, l_Rule,  %g_Pswd_Filename%, %g_Key%,Rule
	if ((l_Rule ="") or (l_Rule ="ERROR")) 
		l_Rule = %g_Rule%

	/*
	**************************************************************************************************************************************	
		Password rule:
		   1. Any password created begin with Upper Case, Lower Case, Numeric for the first 3 characters.
		   2. If password length is 6 or 8, then only contain alphabet and numeric
		   3. If password length is 12, then contain alphabet, numeric 
		   4. If password length is 12, then contain alphabet, numeric as well as special characters, like !,@,#,$,%,&,*,(,)
	**************************************************************************************************************************************			
	
	*/ 	
	if (l_Rule =1) 
		length = 6 
	if (l_Rule =2) 
		length = 8	
	if ((l_Rule =3)  or (l_rule=4))
		length = 12  
			
	Random,R1,1,26
	Random,R2,27,52
	Random,R3,53,62	
	C1:=Char_%R1%
	C2:=Char_%R2%
	C3:=Char_%R3%
	pswd =%C1%%C2%%C3%
			
	;~ Leave last char as capital 
	 length -=  4	 	

	
	Loop , %length% {
		if  (l_rule = 4)   	; 12 -3 		
			Random,x,1,71
		else          		   
			Random,x,1,62
		 
		c := char_%x%
				
		StringCaseSense , On
		pswd = %pswd%%c%		
	}
		
		Random,Rx,1,26
		c := char_%Rx%
				
		StringCaseSense , On
		pswd = %pswd%%c%		
		
		IniRead, Mpswd, %MASTER_CONFIG_FILE%, Password, Master		
		
		;~ msgbox  "master password"  %Mpswd%
		Mpswd_de := Crypt.Encrypt.StrDecrypt(Mpswd,PUBLIC,5,1)		
		
		pswd := Crypt.Encrypt.StrEncrypt(pswd,Mpswd_de,5,1) ; encrypts string using AES_128 encryption and MD5 hash
		;msgbox  "New password has been encrypets " %pswd%		
	
}


f_Check_Selection(){
	if  (g_Key < 1) {				; button not pressed correctly,
		MsgBox  请选择一个操作的密码先！
		Gui, Destroy
		return -1
	}
}


f_Send_mail(Subject, Body){		
	IniRead, Toaddress,%MASTER_CONFIG_FILE%, Password, Email
	;~ IniRead, %%,%g_Pswd_Filename%,%g_Key%,			
	
	if ((Toaddress ="") or (Toaddress = "ERROR")) {
		MsgBox email地址获取失败，请设置email先
		Gui, Destroy
		Exit
	}				


/* 	Subject = "Confidential Pin Code"
 * 	IniRead, Body,%g_Pswd_Filename%,%g_Key%				
 * 	AttachPath :=g_Pswd_Filename
 */

	/*
	object.FromAddress := "PasswordMaster@qq.com"
	ToAddress := "szhulei@cn.ibm.com"
	Subject := "MAPI TEST"
	Body := "This is a test of the MAPI hooked batch sender"
	AttachPath := "c:\hul\pswd.ini" ; can add multiple attachments, the delimiter is |
	*/	
	AttachPath = %ENCRYPT_FILE%
	
	ol := ComObjCreate("Outlook.Application")
	ns := ol.getNamespace("MAPI")
	ns.logon("","",true,false) ;get default MAPI profile
	newMail := ol.CreateItem(0) ;0 is Outlook constant 'olMailItem' (thanks Sinkfaze)
	newMail.Subject := Subject
	newMail.Body := Body
	Attach := newMail.Attachments
	Loop, Parse,AttachPath, |, %A_Space%%A_Tab%
	Attach.Add(A_LoopField)

	; validate the recipient, just in case...
	myRecipient := ns.CreateRecipient(ToAddress)
	myRecipient.Resolve
	
	If Not myRecipient.Resolved
		MsgBox "unknown recipient" 联系作者
	Else {
		newMail.Recipients.Add(myRecipient)
		;newMail.Sender := FromAddress ;this would be the from field but does not work yet
		newMail.Send
		MsgBox 邮件发送成功
	}	
	
	FileDelete Attachpath
	Return
}


	
Button确定:
	Gui, Submit  
	if  (g_Function = "") {				; not choose any funtion to perform
		MsgBox 您需要选择一个功能
		goto begin		
	}
		
	;~  g_Function  = 	1	Create
	;~ 					2	Refresh
	;~ 					3	View
	;~ 					4	ViewLast
	;~ 					5 	Update
	;~ 					6 	Remove		
	if  ((g_Function <= 6) ) {
		if  (f_Check_Selection( ) = -1) 			; check opinion
			goto begin
	}
	
	
	;~ Password maintain
		
	/* 	
		===============================================================
					Create  Password
		===============================================================
	*/
	if  (g_Function = FUNTION_CREATE) {
		
		
		if 	((g_Rule = "" ) or (g_Interval = ""))		{
			MsgBox 创建密码前请先选择规则和有效期 
			Gui, Destroy
			goto begin
		}		
		f_create_password() 			; create password
		Gui, Destroy
		Exit
	}		
		
	
	/* 	
		===============================================================
					Refresh  Password
		===============================================================
	*/

	if  (g_Function = FUNTION_REFRESH) {
	
		if (f_Verfiy_Master() = -1)
		goto begin
		
		IniRead, l_Rule,  %g_Pswd_Filename%, %g_Key%,Rule
		if  (l_Rule = "M" ){
			MsgBox 你不能刷新一个手动设置的密码，请使用手动更新		
			goto begin
		}
		
		IniRead, l_interval,  %g_Pswd_Filename%, %g_Key%,Interval
		if  (l_interval = "5" ){
			MsgBox, 4,, 你设置了永不过期 `n   确定你想刷新密码 %g_Key%			
			IfMsgBox  No
				goto begin
		}
			
			
		
		IniRead, l_stamp,%g_Pswd_Filename%,%g_Key%,Stamp				
		if ((l_stamp = "") or (l_stamp = "ERROR")) {
			MsgBox 密码 %g_key% 未被使用
			goto begin
		}		
			
		l_current = %A_Now%
							
			;~ Check if refresh password on the same day which is not allowed.
		EnvSub, l_current, %l_stamp%, days
											
		if  l_current <=5
			MsgBox , 小心！ 你将刷新的密码5天内创建！
			
		
		MsgBox, 4,, 确定刷新密码 %g_Key%
		IfMsgBox  No
			goto begin
				
		IniRead, pswd_prev, %g_Pswd_Filename%,%g_Key%, Password						
		IniRead, stamp_prev, %g_Pswd_Filename%,%g_Key%, Stamp											
			
		if ((pswd_prev != "ERROR") and (pswd_prev !="")){									
			pswdLength := StrLen(pswd_prev)
			IniWrite, %pswd_prev%, %g_Pswd_Filename%,%g_Key%, Prev_password	; save the previous password and get password length
			IniWrite, %stamp_prev%, %g_Pswd_Filename%,%g_Key%, Prev_stamp
		}	
				
		f_Write_Password()
		
		MsgBox  密码  %g_Key% 刷新成功
		
		Gui, Destroy
		Exit
	}


	/* 	
		===============================================================
					View  Password
		===============================================================
	*/

	if  (g_Function = FUNTION_VIEW) {
		
/* 		if (f_Verfiy_Master() = -1)
 * 			goto begin
 */
		
		;~ Description, updated date & time, Previous (interval, ID)
		f_View_Password()				
				
		Gui, Destroy
		Exit
	}


	/* 	
		===============================================================
					View Last Password
		===============================================================
	*/

	if  (g_Function = FUNTION_VIEWLAST) {
	;	
	;	if (f_Verfiy_Master() = -1)
	;		goto begin
		;~ Description, updated date & time, Previous (interval, ID)
		f_Read_PrePassword()				
		Gui, Destroy
		Exit
	}

	/* 	
		===============================================================
					Update Password
		===============================================================
	*/

	if  (g_Function = FUNTION_UPDATE) {
		
		if (f_Verfiy_Master() = -1)
			goto begin
		
		IniRead, l_pswd,%g_Pswd_Filename%,%g_key%,Password
		IniRead, l_decp,%g_Pswd_Filename%,%g_key%,Description
	 	IniRead, l_userid,%g_Pswd_Filename%,%g_key%,Userid
		if  ((l_pswd = "ERROR") or (l_pswd = "" ))  {
			MsgBox  密码未创建
			Gui, Destroy
			goto begin
		}
				
			
		MsgBox, 4,, 确定你希望 覆盖 密码信息？
		IfMsgBox,  No 
			goto begin
				
				
		InputBox, g_Description, 输入新的描述, 输入新的描述, , 200, 150,,,,,%g_Description% %l_decp%
		if  ErrorLevel
			MsgBox, 取消 更新密码信息
		else 
			f_Write_Description()					
			
		InputBox, g_Userid, 输入用户名, 输入用户名, , 200, 150,,,,,%g_Userid% %l_userid%
		if  ErrorLevel
			MsgBox, 取消密码更新 
		else 
			f_Write_Userid()					
			
		MsgBox  密码 %g_Key% 更新成功！
			
		Gui, Destroy
		Exit
	}

	/* 	
		===============================================================
					Remove Password
		===============================================================
	*/
	if  (g_Function = FUNTION_REMOVE) {
		
		if (f_Verfiy_Master() = -1)
			goto begin
		
		IniRead, pswd,  %g_Pswd_Filename%, %g_Key%
		if (pswd ="") {
			MsgBox 密码 %g_key% 未被使用
			exit
		}
			
		MsgBox, 4,, 你想删除密码 %g_key% 所有信息？
		IfMsgBox,  No 	
			exit
			IniDelete, %g_Pswd_Filename%, %g_Key%
		MsgBox  密码 %g_Key% 删除成功！
		Gui, Destroy
		Exit
	}
	
	/* 	
		===============================================================
					View Email
		===============================================================
	*/

	if  (g_Function = FUNTION_VIEWEMAIL) {
		
		IniRead, l_Read_Email, %MASTER_CONFIG_FILE%, Password, Email
		
		If ((l_Read_Email<>"")and (l_Read_Email <>"ERROR")) 
			MsgBox Email是 %l_Read_Email%					
		else 
			MsgBox 请先设置email地址
		Gui, Destroy
		Exit
	}
		
	/* 	
		===============================================================
					Set Email
		===============================================================
	*/

	if  (g_Function = FUNTION_SETEMAIL) {
		
	;	if (f_Verfiy_Master() = -1)
	;		goto begin
			
		IniRead, l_Read_Email, %MASTER_CONFIG_FILE%, Password, Email
		If ((l_Read_Email<>"")and (l_Read_Email <>"ERROR")) {
			MsgBox, 4, , Email 已存在，继续操作将覆盖
			
			IfMsgBox, No
				goto begin
		}	
		
		InputBox, g_email, Enter email, Enter email address , , 150, 150
			
		IniWrite, %g_email%, %MASTER_CONFIG_FILE%, Password, Email
		MsgBox  Email设置成功。请先配置 office outlook 邮件客户端，并设定帐号。详细内容查看说明文档。		
		Gui, Destroy
		Exit
	}
		
		
	/* 	
		===============================================================
					Set Master Password
		===============================================================
	*/
	
	
	if  (g_Function = FUNTION_SETMASTER) {
								
		
			IniRead, l_Read_Master, %MASTER_CONFIG_FILE%, Password, Master
			
			l_Read_Master := Crypt.Encrypt.StrDecrypt(l_Read_Master,PUBLIC,5,1) 
			
			If ((l_Read_Master<>"")and (l_Read_Master <>"ERROR") and (l_Read_Master <> 0) ) {
				MsgBox, 4, , 主密码已经存在， 继续操作将覆盖主密码 
				IfMsgBox, No 
					goto begin
			}	
			
			MsgBox  安全起见，请先备份密码文件！
			InputBox, l_OLD_Master, Verfiy Old, 输入旧主密码, Hide, 150, 150
			;~ verfiy password
			if (l_Read_Master <>l_OLD_Master){
				MsgBox Master Password mismatch.						
				goto begin
			}
			
			InputBox, l_New_Master, 输入密码, 输入主密码.,Hide , 150, 150
			
			
;  		master password can NOT be null.
 			If (l_New_Master = "" ){
  				MsgBox 主密码不能为空
  				goto begin
  			}
 
			
			
			InputBox, l_New_Master_again, Confirm, 再次输入新的主密码 .,Hide , 150, 150
			if (l_New_Master_again<> l_New_Master){
				MsgBox 两次输入不一致，请重试。
				goto begin
			}
			
			l_Masterpswd_enc:= Crypt.Encrypt.StrEncrypt(l_New_Master,PUBLIC,5,1) 					
				
			if ((l_Masterpswd_enc <>"") and (l_Masterpswd_enc <>0))  {
				i = 0	
				;~ decrp & encrp all the Password with the new Master password				
				Loop , %MAX_PASSWORD%{
					i++
					IniRead, l_pswd_enc,%g_Pswd_Filename%,%i%,Password																	
					IniRead, l_prevpswd_enc,%g_Pswd_Filename%,%i%,Prev_password																
					
					if ((l_pswd_enc = "")or(l_pswd_enc = "ERROR"))  
						continue 	 ; password not found											
											
					l_pswd_dec :=  Crypt.Encrypt.StrDecrypt(l_pswd_enc,l_Read_Master,5,1)							
					l_pswd_enc:= Crypt.Encrypt.StrEncrypt(l_pswd_dec,l_New_Master,5,1) 								  		
					if ((l_pswd_enc = 0) or (l_pswd_enc = "") or (l_pswd_enc = "ERROR")) {
						MsgBox 加密失败，请使用之前的备份，或者联系作者						
						goto begin
					}
												
					IniWrite, %l_pswd_enc%, %g_Pswd_Filename%,%i%, Password													
					
					;~ update prev Password
					IniRead, l_prevpswd_enc,%g_Pswd_Filename%,%i%,Prev_password																
					
					if ((l_prevpswd_enc = "")or(l_prevpswd_enc = "ERROR"))  
						continue 	 ; password not found											
					
					l_prevpswd_dec :=  Crypt.Encrypt.StrDecrypt(l_prevpswd_enc,l_Read_Master,5,1)	
					l_prevpswd_enc:= Crypt.Encrypt.StrEncrypt(l_prevpswd_dec,l_New_Master,5,1) 		
					if ((l_prevpswd_enc = 0) or (l_prevpswd_enc = "") or (l_prevpswd_enc = "ERROR")) {
						MsgBox 加密失败，联系作者						
						goto begin
					}
																		
					IniWrite, %l_prevpswd_enc%, %g_Pswd_Filename%,%i%, Prev_password		
				}
				
				IniWrite, %l_Masterpswd_enc%, %MASTER_CONFIG_FILE%, Password, Master
				MsgBox 新密码创建，请牢记并妥善保管！				
			}

	
		Gui, Destroy
		Exit
	}
	
	/* 	
		===============================================================
					Send Email
		===============================================================
	*/

	if  (g_Function = FUNTION_SENDEMAIL) {
		
		;~ Fill the mail
		Subject 	= Backup Pin File
		Body 		=  Hi  `n    You specify send mail to backup your pin file. The file has already been encrpted by 128 bits AES 	
		IniRead, l_Master, %MASTER_CONFIG_FILE%, Password, Master			
		l_Master := Crypt.Encrypt.StrDecrypt(l_Read_Master,PUBLIC,5,1) 	
		
		Crypt.Encrypt.FileEncrypt(g_pswd_filename,ENCRYPT_FILE,l_Master,5,1)					
		
		f_Send_mail(Subject, Body)
		Gui, Destroy
		Exit
	}

	if  (g_Function = FUNTION_DECRYPTFILE) {
		
		;~ decrypt file
		MsgBox, 4, , 选择已加密的文件
		IfMsgBox, No   			; don't continue
			goto begin
			;~ specify the password fie
		
		FileSelectFile, Pswdenc_Filename, 3			
		if  Pswdenc_Filename=		
		{	
			MsgBox, 4,, 密码文件没有指定，重试？ 
			IfMsgBox, No   
				goto begin
			FileSelectFile, Pswdenc_Filename, 3	
			if  Pswdenc_Filename = 
				goto begin
		}
						
		MsgBox, 4, , 请指定密码文件
		IfMsgBox, No   			; don't continue
			goto begin
			;~ specify the password fie
		
		FileSelectFile, Pswddec_Filename, S24			
		if  Pswddec_Filename=		
		{	
			MsgBox, 4,, 解密文件未被指定？重试？ 
			IfMsgBox, No   
				goto begin
			FileSelectFile, Pswddec_Filename, 	S24
			if  Pswddec_Filename = 
				goto begin
		}
		
		IniRead, l_Master, %MASTER_CONFIG_FILE%, Password, Master			
		l_Master := Crypt.Encrypt.StrDecrypt(l_Read_Master,PUBLIC,5,1) 	
		
		if (Crypt.Encrypt.FileDecrypt(Pswdenc_Filename,Pswddec_Filename,l_Master,5,1)	<= 0) 
			MsgBox 解密失败，联系作者
		
	
		Gui, Destroy
		Exit
	}
	
	/* 	
		===============================================================
					 Set password
		===============================================================
	*/
	if  (g_Function = FUNTION_SET) {		
		MsgBox  你将手动设置密码，如非必要请使用"创建" 创建随机密码
		
		if 	 (g_Interval = "")		{
			MsgBox 选择密码有效期
			Gui, Destroy
			goto begin
		}		
		f_set_password() 			; create password
		Gui, Destroy
		Exit
	}	

Button帮助:
	f_display_help()
	goto begin
Exit

/*
Buttondhcp:				
		
	RunWait  netsh interface ip set dns Wireless source=static addr=9.0.148.50		
	dns = att
	Gui, Destroy
	Exit
	

Buttonatt:			
	
		
	RunWait netsh interface ip set dns Wireless source=dhcp
	dns = dhcp
	Gui, Destroy
	Exit
		
		
*/

Button查看:
	IniRead, g_Pswd_Filename, %MASTER_CONFIG_FILE%, Password, Directroy
	
	IniRead, l_Read_Email, %MASTER_CONFIG_FILE%, Password, Email
		

	MsgBox 	Password directory  --- %g_Pswd_Filename%          `n                                    Email Address  -------- %l_Read_Email%			
	
	Gui, Destroy
	Exit	
	goto begin    

Button设置:
		FileSelectFile, g_Pswd_Filename, ,%g_Pswd_Filename%
		if  (g_Pswd_Filename = 	){
			MsgBox 您取消了密码文件的选择
			Exit
		}
			
	
	;~ write password file name to config file	
	IniWrite, %g_Pswd_Filename%, %MASTER_CONFIG_FILE%, Password, Directroy
	Gui, Destroy
	Exit	
	goto begin 




GuiClose:
ButtonMin:
GuiEscape:
	;~ MsgBox Use Win+P to invoke this panel again.
	Gui, Destroy
	Exit
	
	
MainPanel:
	goto begin
	exit 

  
 About:
	MsgBox 	Password Manager V2.0 `n   	Author: Clark Hu     `n    23465446@qq.com 
	MsgBox 	该软件用于方便日常密码管理，使用者需要定期对密码文件备份，并妥善保管文件。 如有意外发生，作者不承担任何责任。
	Exit

Help:
	f_display_help()
	goto begin
Exit


Exit:
	ExitApp
	
	
	
	


